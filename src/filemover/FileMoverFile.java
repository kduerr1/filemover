/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import java.io.File;

/**
 *
 * @author kduerr1
 */
public class FileMoverFile {

    private final File file;
    private final FileType fileType;
    private File newDestination;
    private File newPath;
    private File newDirectory;
    private boolean move;
    private boolean isPhenoDBFile = false;

    public FileMoverFile(File file, FileType fileType) {
        this.file = file;
        this.fileType = fileType;
    }

    public File getFile() {
        return file;
    }

    public FileType getFileType() {
        return fileType;
    }

    public File getDirectory() {
        return new File(file.toString().substring(0, file.toString().lastIndexOf("\\")));
    }

    public File getNewDestination() {
        return newDestination;
    }

    public File getNewDirectory() {
        return newDirectory;
    }

    public File getNewPath() {
        return newPath;
    }

    public void setNewDestination(File newDestination) {
        this.newDestination = newDestination;
    }

    public void setNewDirectory(File newDirectory) {
        this.newDirectory = newDirectory;
    }

    public void setNewPath(File newPath) {
        this.newPath = newPath;
    }

    public void replaceNewDestination() {
        newDestination = file;
    }

    public void setMove(boolean move) {
        this.move = move;
    }

    public boolean isMove() {
        return move;
    }

    public void setIsPhenoDBFile(boolean isPhenoDBFile) {
        this.isPhenoDBFile = isPhenoDBFile;
    }

    public boolean isPhenoDBFile() {
        return isPhenoDBFile;
    }
    
    
}
