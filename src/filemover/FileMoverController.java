package filemover;

import com.google.common.base.Joiner;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.dialog.JFXDialogBuilder;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser.FileBrowser;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser.KnownFileType;
import edu.jhmi.cidr.lib.file.FileUtils;
import filemover.util.*;

import java.io.*;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileMoverController {

    public static final boolean TESTING_MODE = false; //copies files and does not delete them for clean ups

    private static final String OLD_MENDEL_DESTNIATION_BROSWE_PATH = "\\\\isilon-cifs.cidr.jhu.edu\\sequencing\\Seq_Proj";
    private static final String MENDEL_DESTNIATION_BROSWE_PATH = "R:\\active";
    private static final String EXCLUSION_PATH = "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\lab_files\\sequencing";

    public static final String COMPLETED_FILE_NAME = "Completed.txt";
//    public static final String COMPLETED_FASTQ_FILE_NAME = "FASTQ_Clean_Up_Completed.txt";

    public static final String COMPLETED_BCL_FILE_NAME = "BCL_Clean_Up_Completed.txt";

    public static boolean movingOrCopying = false;
    private Set<FileMoverFile> allFileMoverFilesList = new HashSet<>();
    private final Deque<Job> jobQue = new LinkedList<>();
    private final Deque<Job> jobHistory = new LinkedList<>();
    private final int count = 0;
    private File exclusionFile;
    private File customCleanUpDir = null;
    private List<File> directoryList = new ArrayList<>();
    private List<String> errorList = new ArrayList<>();
    private List<String> validSmTags;
    private List<String> cleanUpStrings;
    private Map<FileType, Integer> fileTypeCountMap = new HashMap<>();
    private Map<String, Set<FileMoverFile>> directoryToFileMoverFiles = new HashMap<>();
    private Map<String, List<String>> projectToFamilyMap;
    private Map<String, List<String>> projectToSampleMap;
    private Map<String, List<String>> fastqMap;
    private HashMap<String, BooleanProperty> selectedDirectoriesBooleanMap;
    private HashSet<String> cleanUpSelectedDirs;
    private boolean isInitialDirectoryAddition = true;
    private boolean selectAllUsed = false;
    private boolean applicationRunning = true;
    private boolean processAborted = false;
    private boolean move = false;
    private boolean copy = false;
    private boolean selectAll = false;
    private boolean revertRequested = false;
    private int dirCounter = 0;
    private String selectedDir;
    private ListView<String> dirListView = new ListView<>();
    private FileBrowser fileBrowser;
    private MoveMode currentMoveMode;
    //    private GuiDialog guiDialog;
    private Thread fileSeekingThread;
    public static Stage stage;
    private DirectoryHandler directoryHandler;
    private TreeView<?> directoryTreeView;
    private FamilyDirectoryFileSorter familyDirectoryFileSorter;
    private MasterKeyReportHandler masterKeyReportHandler;
    private QcReportHandler qcReportHandler;
    private FileMoverSubmission fileMoverSubmission;
    private FileMoverModeSetup setup;

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Pane ModePane;
    @FXML
    private RadioButton copyRadioButton;
    @FXML
    private TextField reportTextField;
    @FXML
    private Button reportBrowseButton;
    @FXML
    private Label directoryLabel;
    @FXML
    private TextField destinationTextField;
    @FXML
    private Label destinationIntroLabel;
    @FXML
    private Button fileLocationBrowseButton;
    @FXML
    private TextField fileLocationTextField;
    @FXML
    private Button masterKeyBrowseButton;
    @FXML
    private Label masterKeyLabel;
    @FXML
    private TextField masterKeyTextField;
    @FXML
    private Label familiesPrefixLabel;
    @FXML
    private TextField familiesPrefixTextField;
    @FXML
    private Button createReportButton;
    @FXML
    private Pane directoryPane;
    @FXML
    private Label fileLocationLabel;
    @FXML
    private ComboBox<MoveMode> modeComboBox;
    @FXML
    private RadioButton moveRadioButton;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private GridPane fileTypeGridPane;
    @FXML
    private Pane userInputPane;
    @FXML
    private Label currentProgressFileLabel;
    @FXML
    private Label percentProgressLabel;
    @FXML
    private Button submitButton;
    @FXML
    public ProgressBar directoryProgressBar;
    @FXML
    public ProgressBar fileTypeProgressBar;

    @FXML
    void initialize() {
        populateComboBox();
        addEventHandlers();
        setBottomFieldsDisabled(true);
        fileBrowser = new FileBrowser(stage, new File(MENDEL_DESTNIATION_BROSWE_PATH));

        fileSeekingThread = new Thread(() -> {
            while (applicationRunning) {
                if (!jobQue.isEmpty()) {
                    Job job = jobQue.removeFirst();
                    if (job.isCancelJob()) {
                        continue;
                    }
                    job.run();
                } else {
                    try {
                        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                            masterKeyBrowseButton.setDisable(false);
                            dirListView.setDisable(false);
                            masterKeyTextField.setDisable(false);
//                            familiesPrefixTextField.setDisable(false);
                            if (fileMoverSubmission != null) {
                                if (fileMoverSubmission.isFinishedRun()) {
                                    fileMoverSubmission.setFinishedRun(false);
                                    Platform.runLater(() -> {
                                        allFileMoverFilesList.clear();
                                        fillCleanUpFolderList();
                                        createFileTypeBoxes();
                                    });
                                }
                            }
                        }
                        directoryProgressBar.setVisible(false);
                        fileTypeProgressBar.setVisible(false);
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        System.out.println("SLEEPING");
                    }
                }
            }
        });
        fileSeekingThread.start();
    }


    @FXML
    void onModeComboBoxPressed(ActionEvent event) {
        resetFileMover();
        if (currentMoveMode != null) {

            addfFileTypeBoxesToGridPane(currentMoveMode.getFileTypeSet());

            setup = new FileMoverModeSetup(currentMoveMode);
            setup.setObjects(directoryProgressBar, directoryPane, destinationIntroLabel,
                    reportTextField, modeComboBox, masterKeyLabel, familiesPrefixLabel,
                    destinationTextField, directoryLabel, fileLocationBrowseButton,
                    fileLocationTextField, moveRadioButton, copyRadioButton,
                    masterKeyBrowseButton, masterKeyTextField, familiesPrefixTextField,
                    percentProgressLabel, currentProgressFileLabel, progressBar, ModePane,
                    submitButton, userInputPane, fileLocationLabel, reportBrowseButton,
                    dirListView, createReportButton);

            Platform.runLater(() -> {
                setup.activate();
                customCleanUpDir = setup.getCustomCleanUpDir();

                if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                    fillCleanUpFolderList();
                }

                if (currentMoveMode != null) {
                    for (FileType fileType : currentMoveMode.getFileTypeSet()) {
                        fileTypeCountMap.put(fileType, 0);
                    }
                }
            });
        }
    }

    @FXML
    void moveRadioButtonPressed(ActionEvent event) {
        moveRadioButton.selectedProperty().set(move = true);
        copyRadioButton.selectedProperty().set(copy = false);
    }

    @FXML
    void copyRadioButtonPressed(ActionEvent event) {
        moveRadioButton.selectedProperty().set(move = false);
        copyRadioButton.selectedProperty().set(copy = true);
    }

    @FXML
    void onReportBrowseButtonPressed(ActionEvent event) throws Exception {
        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            fileBrowser = new FileBrowser(stage, new File(EXCLUSION_PATH));

            fileBrowser.openFile(reportTextField, "Select an exclusion file", KnownFileType.CSV);
            exclusionFile = new File(reportTextField.getText());
            if (!exclusionFile.isFile()) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);

                alert.setContentText("Please select an exclusion File");
                alert.setHeaderText("Exclusion File Missing");
                alert.setTitle("Warning");

                if (exclusionFile.getAbsolutePath().equals("")) {
                    alert.showAndWait().ifPresent(response -> {
                        if (response == ButtonType.OK) {
                        }

                    });
                }
                reportTextField.getStylesheets().clear();
                reportTextField.setText("");
            } else {
                reportTextField.setId("reportTextField");
                reportTextField.getStylesheets().add(this.getClass().getResource("reportTextField.css").toExternalForm());
            }

        } else {

            allFileMoverFilesList = new HashSet<>();
            setup.resetUserInputPane();
            fileBrowser.openFile(reportTextField, "Select a File", KnownFileType.CSV);
            qcReportHandler = new QcReportHandler(reportTextField, stage);
            qcReportHandler.parseQcReport();
            validSmTags = new ArrayList<>();
            validSmTags = qcReportHandler.getQcReportSmTags();
            if (currentMoveMode.equals(MoveMode.QC_REPORT)) {
                if (new File(reportTextField.getText()).isFile()) {
                    setup.setReportFieldsVisible(true);
                    setup.setFamilyFieldsVisible(false);
                    directoryProgressBar.setOpacity(1);
                    setup.setBottomFieldsDisabled(false);
                }
            }
            if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                if (new File(reportTextField.getText()).isFile()) {
                    setup.setBottomFieldsDisabled(false);
                    setup.setReportFieldsVisible(true);
                    setup.setFamilyFieldsVisible(true);
                    directoryProgressBar.setOpacity(0);
                }
            }
            if (qcReportHandler.isMissingSmTags()) {
                setup.setBottomFieldsDisabled(true);
            }
            projectToSampleMap = qcReportHandler.getProjectToSampleMap();
            directoryHandler = new DirectoryHandler(allFileMoverFilesList, directoryTreeView, directoryPane, fileTypeGridPane, currentMoveMode, jobQue, fileTypeProgressBar, directoryProgressBar);

            if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                fileLocationTextField.setText(MENDEL_DESTNIATION_BROSWE_PATH);
            }
            createNewJob(fileLocationTextField.getText());
        }
    }

    @FXML
    void onFileLocationBrowseButtonPressed(ActionEvent event) {
        allFileMoverFilesList = new HashSet<>();
        fileBrowser.selectFolder(fileLocationTextField, "Select a Folder");
        String fileLocationString = fileLocationTextField.getText().substring(fileLocationTextField.getText().lastIndexOf("\\") + 1, fileLocationTextField.getText().length());
        if (fileLocationTextField.getText().endsWith("\\")) {
            String intermediateString = fileLocationTextField.getText().substring(0, fileLocationTextField.getText().length() - 1);
            fileLocationString = intermediateString.substring(intermediateString.lastIndexOf("\\") + 1, intermediateString.length());
        }
        fileLocationLabel.setText(fileLocationString);
        directoryHandler = new DirectoryHandler(allFileMoverFilesList, directoryTreeView, directoryPane, fileTypeGridPane, currentMoveMode, jobQue, fileTypeProgressBar, directoryProgressBar);
        createNewJob(fileLocationTextField.getText());
    }

    @FXML
    void onCreateReportButtonPressed(ActionEvent event) {
        CleanupReportBuilder reportBuilder = new CleanupReportBuilder(stage, currentMoveMode, cleanUpStrings);
        reportBuilder.writeReport();
    }

    public Map<String, List<String>> getDataFromCSV(final String filePath) throws IOException {
        fastqMap = new HashMap<>();


//        List<List<String>> records = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {

                String[] values = line.split(",");

                List<String> datas = new ArrayList<>();
                if (fastqMap.containsKey(values[3])) {
                    datas = fastqMap.get(values[3]);
                }
                datas.add(values[0] + "_" + values[1] + "_" + values[2]);

                if (!values[0].equals("Flowcell")) {
                    fastqMap.put(values[3], datas);
                }
//                records.add(Arrays.asList(values));
            }
        }
        return fastqMap;
    }

    @FXML
    void onMasterKeyBrowseButtonPressed(ActionEvent event) throws Exception {

        allFileMoverFilesList = new HashSet<>();
        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
            fileBrowser.openFile(masterKeyTextField, "Select a master key file", KnownFileType.CSV);
            masterKeyReportHandler = new MasterKeyReportHandler(masterKeyTextField, stage);
            masterKeyReportHandler.parseMasterKeyReport();

            projectToFamilyMap = masterKeyReportHandler.getProjectToFamilyMap();
            directoryHandler = new DirectoryHandler(allFileMoverFilesList, directoryTreeView, directoryPane, fileTypeGridPane, currentMoveMode, jobQue, fileTypeProgressBar, directoryProgressBar);
            createNewJob(fileLocationTextField.getText());

        } else if (currentMoveMode.equals(MoveMode.FAST_Q_CLEAN_UP)) {
            fileBrowser = new FileBrowser(stage, new File(currentMoveMode.getInitialDirectory()));

            fileBrowser.openFile(masterKeyTextField, "Select a FASTQ inclusion file", KnownFileType.CSV);

            Map<String, List<String>> inclusionMap = getDataFromCSV(masterKeyTextField.getText());
            int samplecount = 0;
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<String, List<String>> entry : inclusionMap.entrySet()) {
                samplecount = samplecount + entry.getValue().size();
                stringBuilder.append(entry.getKey() + " : " + entry.getValue().size());
                stringBuilder.append("\n");
            }


            Set<String> filteredDirs = new TreeSet<>();
            Set<String> inclusionflowcells = new HashSet<>();

            dirListView.getItems().forEach(s -> {
                inclusionMap.values().forEach(strings -> {
                    strings.forEach(s1 -> {
                        inclusionflowcells.add(s1.substring(0, s1.indexOf("_")));
                    });
                });
            });
            dirListView.getItems().forEach(s -> {
                inclusionflowcells.forEach(s1 -> {
                    if(s.contains(s1)) {
                        filteredDirs.add(s);
                    }
                });

            });
            dirListView.getItems().setAll(filteredDirs);

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Found Samples");
            alert.setContentText(samplecount + " Samples Found" + "\n"
                    + inclusionMap.size() + " Projects Found" + "\n"
                    + inclusionflowcells.size() + " Flowcells Found" + "\n\n"
                    + stringBuilder);

            alert.show();
            //DISPLAY PROJECT TOTAL AND INDEX TOTAL IN ALERT?

        } else {
            //CLEAN_UP MODE (Seq_Run Folder Browse Pressed)
            fileBrowser = new FileBrowser(stage, new File(currentMoveMode.getInitialDirectory()));

            if (MoveMode.CUSTOM_BCL_CLEAN_UP.equals(currentMoveMode)) {
                fileBrowser.selectFolder(masterKeyTextField, "Select a BCL Clean up Folder");
            } else {

                fileBrowser.selectFolder(masterKeyTextField, "Select a Run Folder");
                if (!masterKeyTextField.getText().isEmpty()) {
                    File directory = new File(masterKeyTextField.getText());
                    if (!directory.getParent().equals(currentMoveMode.getInitialDirectory()) || !selectedDirectoriesBooleanMap.containsKey(directory.getName())) {

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);

                        alert.setContentText("Please select an available clean up directory");
                        alert.setHeaderText("Clean Up Directory Required");
                        alert.setTitle("Missing Required Information");
                        alert.showAndWait().ifPresent(response -> {
                            if (response == ButtonType.OK) {
                                masterKeyTextField.clear();
                            }

                        });
                    }
                }
            }
        }
    }

    @FXML
    void onDestinationBrowseButtonPressed(ActionEvent event) {
        fileBrowser.selectFolder(destinationTextField, "Select a destination to save files");
    }

    @FXML
    void onSubmitButtonPressed(ActionEvent event) throws Exception {
        validateUserInput();
        processAborted = false;
        movingOrCopying = true;
        final String initialDirectoryString = new File(fileLocationTextField.getText()).toString();
        final File selectedDestination = new File(destinationTextField.getText()).getAbsoluteFile();
        fileMoverSubmission = new FileMoverSubmission(fileTypeGridPane, directoryHandler,
                stage, anchorPane, userInputPane, submitButton, ModePane, currentProgressFileLabel,
                percentProgressLabel, progressBar, directoryToFileMoverFiles, fileTypeCountMap);
        fileMoverSubmission.setUserData(currentMoveMode, move, copy, initialDirectoryString, selectedDestination, allFileMoverFilesList, masterKeyReportHandler, familyDirectoryFileSorter, projectToFamilyMap, cleanUpSelectedDirs, customCleanUpDir);
        if (!errorList.isEmpty()) {
            fileMoverSubmission.setCompleted(true);
            Alert submitAlert = new Alert(Alert.AlertType.ERROR);

            submitAlert.setContentText(Joiner.on("\n").join(errorList));
            submitAlert.setHeaderText("Submission Errors");
            submitAlert.setTitle("Error");
            submitAlert.showAndWait();
            errorList.clear();
        } else {

            final GuiLoadScreen loadScreen = new GuiLoadScreen(stage);
            loadScreen.show("Please wait for confirmation ...");
            Thread thread = new Thread(() -> {
                final List<FileMoverFile> files = fileMoverSubmission.checkForDuplicates();
                final List<String> missingMD5s = fileMoverSubmission.getBamFilesMissingMD5s();

                Platform.runLater(() -> {
                    loadScreen.hide();
                    if (!missingMD5s.isEmpty()) {
                        Alert submitAlert = new Alert(Alert.AlertType.ERROR);

                        submitAlert.setContentText("Missing MD5s:\n" + String.join("\n", missingMD5s));
//                            submitAlert.setHeaderText("Overwrite Files?");
                        submitAlert.setTitle("Missing MD5s");
                        submitAlert.show();
                    } else {

                        if (!files.isEmpty()) {
                            String duplicatesError = (files.size() + " of the selected files already exist in\n" + selectedDestination + ".\nOverwrite these files?");

                            Alert submitAlert = new Alert(Alert.AlertType.CONFIRMATION);

                            submitAlert.setContentText(duplicatesError);
                            submitAlert.setHeaderText("Overwrite Files?");
                            submitAlert.setTitle("Confirm Overwrite");
                            submitAlert.showAndWait().ifPresent(response -> {
                                if (response == ButtonType.OK) {
                                    movingOrCopying = true;
                                    fileMoverSubmission.submit();
                                }
                                if (response == ButtonType.CANCEL) {
                                }
                            });

                        } else {
                            movingOrCopying = true;
                            fileMoverSubmission.submit();
                        }
                    }
                });
            });
            thread.start();
        }
    }

    public boolean shutdown() {
        if (movingOrCopying) {
            if (!fileMoverSubmission.isCompleted()) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.getButtonTypes().add(ButtonType.CLOSE);

                alert.setContentText("Please wait for process to complete");
                alert.setHeaderText("Task Running");
                alert.setTitle("Cancel Task");

                if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                    if (fileMoverSubmission.isRunning()) {
                        alert.showAndWait().ifPresent(response -> {
                            if (response == ButtonType.OK) {
                            }
                            if (response == ButtonType.CLOSE) {
                                System.exit(0);
                                Platform.exit();
                            }
                        });

                        return false;
                    } else {
                        return true;
                    }
                } else {
                    fileMoverSubmission.setAbort(true);
                    if (!processAborted && !revertRequested) {
                        Alert abortAlert = new Alert(Alert.AlertType.WARNING);
                        abortAlert.getButtonTypes().add(ButtonType.CLOSE);
                        abortAlert.setContentText(fileMoverSubmission.getSuccessfulFileMoverFileList().size() + " files processed\nChanges will be reverted");
                        abortAlert.setHeaderText("Revert Task?");
                        abortAlert.setTitle("Revert Task");

                        abortAlert.showAndWait().ifPresent(response -> {
                            revertRequested = true;
                            if (response == ButtonType.OK) {
                                processAborted = true;
                                movingOrCopying = true;
                                String currentProgressFileString;
                                int fileCount = 0;
                                int completeFileCount = fileMoverSubmission.getSuccessfulFileMoverFileList().size();
                                for (FileMoverFile fileMoverFile : fileMoverSubmission.getSuccessfulFileMoverFileList()) {
                                    fileCount++;
                                    try {
                                        if (fileMoverFile.isMove()) {
                                            fileMoverFile.getNewPath().renameTo(fileMoverFile.getFile());
                                        } else {
                                            fileMoverFile.getNewPath().delete();
                                        }
                                        fileMoverSubmission.updateProgressLabel();
                                        progressBar.setProgress(fileCount / completeFileCount);
                                        currentProgressFileString = fileMoverFile.getFile().getName();
                                        fileMoverSubmission.setCurrentProgressFileString(currentProgressFileString);
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                                Platform.runLater(() -> {
                                    Alert revertAlert = new Alert(Alert.AlertType.INFORMATION);
//                                    revertAlert.getButtonTypes().add(ButtonType.CLOSE);
                                    revertAlert.setContentText(fileMoverSubmission.getSuccessfulFileMoverFileList().size() + " files reverted");
                                    revertAlert.setHeaderText("Revert Complete");
                                    revertAlert.setTitle("Revert Task");

                                    revertAlert.showAndWait().ifPresent(revertResponse -> {
                                        if (revertResponse == ButtonType.OK) {
                                            setup.setAllFieldsDisabled(false);
                                            anchorPane.setCursor(Cursor.DEFAULT);
                                            movingOrCopying = false;
                                            revertRequested = false;
                                            fileMoverSubmission.setAbort(false);
                                        }

                                    });
                                });
                            }
                            if (response == ButtonType.CLOSE) {
                                System.exit(0);
                                Platform.exit();
                            }
                        });
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void validateUserInput() {
        UserInputValidator userInputValidator = new UserInputValidator(
                currentMoveMode, fileTypeProgressBar, copy, move, reportTextField,
                fileLocationTextField, destinationTextField, masterKeyTextField, cleanUpSelectedDirs);

        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            if (cleanUpSelectedDirs.isEmpty()) {
                errorList.add("No selected directories");
            }
        }
        if (!userInputValidator.isValid()) {
            errorList = userInputValidator.getErrorList();
        }
    }

    private void addfFileTypeBoxesToGridPane(Set<FileType> fileTypes) {
        GridPaneFiller paneFiller = new GridPaneFiller(fileTypeGridPane, currentMoveMode, fileTypes);
        paneFiller.fillPane();
    }

    private void addEventHandlers() {
        fileLocationTextField.addEventHandler(KeyEvent.ANY, (KeyEvent t) -> {
            allFileMoverFilesList.clear();
            String fileLocationString = fileLocationTextField.getText().substring(fileLocationTextField.getText().lastIndexOf("\\") + 1, fileLocationTextField.getText().length());
            if (fileLocationTextField.getText().endsWith("\\")) {
                String intermediateString = fileLocationTextField.getText().substring(0, fileLocationTextField.getText().length() - 1);
                fileLocationString = intermediateString.substring(intermediateString.lastIndexOf("\\") + 1, intermediateString.length());
            }
            fileLocationLabel.setText(fileLocationString);
            if (!new File(fileLocationTextField.getText()).isDirectory()) {
                fileLocationLabel.setStyle("-fx-text-fill: RED");
                directoryProgressBar.setVisible(true);
                fileTypeProgressBar.setVisible(true);
            } else {
                directoryProgressBar.setVisible(false);
                fileTypeProgressBar.setVisible(false);
                fileLocationLabel.setStyle("-fx-text-fill: GREEN");
            }
            clearFileTypeCountMap();
            createNewJob(fileLocationTextField.getText());
        });

        masterKeyTextField.textProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            if (!currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                //TODO fix search function with selection
                File pickedDir = new File(masterKeyTextField.getText());
                if (pickedDir.isDirectory()) {
                    for (String string : dirListView.getItems()) {
                        selectedDirectoriesBooleanMap.get(string).setValue(false);
                    }
                    if (selectedDirectoriesBooleanMap.containsKey(pickedDir.getName())) {
                        sortDirectoryListView(newValue, true);
                    }
                } else {
                    sortDirectoryListView("", false);
                }
            }
        });

        familiesPrefixTextField.textProperty().addListener((ObservableValue<? extends String> ov, String oldValue, String newValue) -> {
            if (!currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                if (masterKeyTextField.getText().isEmpty()) {
                    sortDirectoryListView(newValue, false);
                }
            }
        });
        reportTextField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                    if (reportTextField.getText().equals("")) {
                        exclusionFile = null;
                        reportTextField.getStylesheets().clear();
                        for (String dir : cleanUpStrings) {
                            if (!dirListView.getItems().contains(dir)) {
                                dirListView.getItems().add(dir);
                            }
                        }
                    } else {
                        File newExclusionFile = new File(reportTextField.getText());
                        if (newExclusionFile.isFile()) {
                            exclusionFile = newExclusionFile;
                            reportTextField.getStylesheets().add(this.getClass().getResource("reportTextField.css").toExternalForm());
                            ExclusionFileParser exclusionFileParser = new ExclusionFileParser(allFileMoverFilesList, exclusionFile);
                            exclusionFileParser.start();
                            for (String string : exclusionFileParser.getExcludedFolderNames()) {
                                if (dirListView.getItems().contains(string)) {
                                    dirListView.getItems().remove(string);
                                }
                            }
                        } else {
                            reportTextField.getStylesheets().clear();
                            exclusionFile = null;
                        }
                    }
                    createFileTypeBoxes();
                }
            }
        });
    }

    private void sortDirectoryListView(String newValue, boolean isSort) {
        if (!newValue.equals("")) {
            List<String> sortedDirs = new ArrayList<>();
            for (String dir : selectedDirectoriesBooleanMap.keySet()) {
                if (dir.toLowerCase().contains(newValue.substring(newValue.lastIndexOf("\\") + 1, newValue.length()).toLowerCase())) {
                    sortedDirs.add(dir);
                    if (isSort) {
                        selectedDirectoriesBooleanMap.get(dir).setValue(true);
                    }
                }
            }
            Collections.sort(sortedDirs);
            dirListView.getItems().setAll(sortedDirs);
        } else {
            Collections.sort(cleanUpStrings);
            dirListView.getItems().setAll(cleanUpStrings);
        }
    }

    private void createNewJob(String fileLocation) {
        if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            for (Job job : jobHistory) {
                job.cancelJob();
            }
        } else {
            for (Job job : jobHistory) {
                if (customCleanUpDir != null) {
                    if (job.getDirectory().equals(customCleanUpDir.getAbsolutePath())) {
                        selectAllUsed = true;
                    }
                }
                if (job.getDirectory().equals(currentMoveMode.getInitialDirectory())) {
                    selectAllUsed = true;
                }
                if (job.getDirectory().equals(fileLocation) || selectAllUsed) {
                    return;
                }
            }
        }
        if (new File(fileLocation).isDirectory() || !cleanUpSelectedDirs.isEmpty()) {
            final Job newJob = new Job(fileLocation, new Job.JobRunnable() {
                @Override
                public void run(Job newJob) {
                    if (fileTypeProgressBar.isVisible() == false) {
                        directoryProgressBar.setVisible(true);
                        fileTypeProgressBar.setVisible(true);
                    }
                    ModePane.setDisable(true);
                    try {
                        File directory = null;
                        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                            directory = new File(MENDEL_DESTNIATION_BROSWE_PATH);
                        } else {
                            directory = new File(newJob.getDirectory());
                        }
                        if (directory.isDirectory()) {
                            Set<FileMoverFile> fileMoverFiles;
                            if (selectAll) {
                                fileMoverFiles = createFileMoverFilesListFromDirectories(getSelectedDirectories(), allFileMoverFilesList, newJob);
                            } else {
                                fileMoverFiles = createFileMoverFilesListFromDirectory(directory, allFileMoverFilesList, newJob);
                            }

                            allFileMoverFilesList.addAll(fileMoverFiles);
                            isInitialDirectoryAddition = true;
                            directoryHandler.setCheckedDirectoryList(directoryList);

                            if (currentMoveMode.equals(MoveMode.ALL_FILES)
                                    || currentMoveMode.equals(MoveMode.QC_REPORT)) {
                                directoryHandler.createTree(directory, newJob);
                            } else if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                                if (new File(masterKeyTextField.getText()).isFile()) {
                                    Platform.runLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            List<String> masterKeySmTags = new ArrayList<>();
                                            for (String smTag : masterKeyReportHandler.getSmTags()) {
                                                masterKeySmTags.add(smTag);
                                            }
                                            for (String smTag : qcReportHandler.getQcReportSmTags()) {
                                                if (!masterKeySmTags.contains(smTag)) {

                                                    Alert alert = new Alert(Alert.AlertType.INFORMATION);

                                                    alert.setContentText("Master Key Report does not match QC Report\n Please select another Master Key Report");
                                                    alert.setHeaderText("Master Key Report Error");
                                                    alert.setTitle("Master Key Report Error");
                                                    alert.showAndWait().ifPresent(response -> {
                                                        if (response == ButtonType.OK) {
                                                            masterKeyTextField.clear();
                                                            directoryPane.getChildren().clear();
                                                            return;
                                                        }
                                                    });
                                                }
                                            }
                                            directoryHandler.populateDirectoryPaneWithProjects(projectToSampleMap, projectToFamilyMap);
                                        }
                                    });
                                }
                            }
                            if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                                clearFileTypeCountMap();
                            }
                            List<FileMoverFile> filesToRemove = new ArrayList<>();
                            for (FileMoverFile fileInList : allFileMoverFilesList) {
                                if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                                    if (isValidMendelFile(fileInList)) {
                                        int count = fileTypeCountMap.get(fileInList.getFileType());
                                        fileTypeCountMap.put(fileInList.getFileType(), count + 1);
                                    } else {
                                        filesToRemove.add(fileInList);
                                    }
                                } else if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                                    if (fileInList.getFile().isFile()) {
                                        int count = fileTypeCountMap.get(fileInList.getFileType());
                                        fileTypeCountMap.put(fileInList.getFileType(), count + 1);
                                    }
                                }
                            }
                            directoryHandler.setMovingFileList(allFileMoverFilesList);
                            allFileMoverFilesList.removeAll(filesToRemove);

                            if (!newJob.isCancelJob()) {
                                if (selectedDir != null && selectAll == false) {
                                    directoryToFileMoverFiles.put(selectedDir, fileMoverFiles);
                                }
                                createFileTypeBoxes();
                            }
                        }
                    } catch (Exception ri) {
                        ri.printStackTrace();
                        System.out.println("INTERUPTED: CLOSING APPLICATION!");
                        Platform.exit();
                    }
                }
            });
            jobQue.add(newJob);
            jobHistory.add(newJob);
        }
    }

    private void createFileTypeBoxes() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                final GuiLoadScreen loadScreen = new GuiLoadScreen(stage);
                loadScreen.show();
                if (directoryHandler != null) {
                    Set<FileMoverFile> allFiles;
                    clearFileTypeCountMap();
                    if (exclusionFile != null) {
                        ExclusionFileParser exclusionFileParser = new ExclusionFileParser(allFileMoverFilesList, exclusionFile);
                        exclusionFileParser.start();
                        for (Map.Entry<String, BooleanProperty> entry : selectedDirectoriesBooleanMap.entrySet()) {
                            String string = entry.getKey();
                            BooleanProperty booleanProperty = entry.getValue();
                            for (String fileMoverFile : exclusionFileParser.getExcludedFolderNames()) {
                                if (fileMoverFile.equals(string)) {
                                    booleanProperty.set(false);
                                }
                            }
                        }
                        exclusionFileParser.getExcludedFiles();
                        allFiles = exclusionFileParser.getFilesToDelete();
                    } else {
                        allFiles = allFileMoverFilesList;
                    }
                    for (FileMoverFile fileMoverFile : allFiles) {
                        fileTypeCountMap.put(fileMoverFile.getFileType(), fileTypeCountMap.get(fileMoverFile.getFileType()) + 1);
                    }
                    directoryProgressBar.setVisible(false);
                    ModePane.setDisable(false);
                    for (Node node : fileTypeGridPane.getChildren()) {
                        FileTypeBox fileTypeBox = (FileTypeBox) node;
                        if (fileTypeCountMap.keySet().contains(fileTypeBox.getFileType())) {
                            directoryHandler.setFileTypeBoxCount(fileTypeBox, fileTypeCountMap.get(fileTypeBox.getFileType()));
                        }
                    }
                }
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        loadScreen.hide();
                    }
                });
            }
        });
    }

    private Set<FileMoverFile> createFileMoverFilesListFromDirectories(List<File> directoryList, Set<FileMoverFile> allFileMoverFilesList, Job newJob) {
        Set<FileMoverFile> moverFiles = new HashSet<>();
        final int totalCount = directoryList.size();

        for (File file : directoryList) {
            if (!directoryToFileMoverFiles.containsKey(file.getName())) {
                Set<FileMoverFile> files = createFileMoverFilesListFromDirectory(file, allFileMoverFilesList, newJob);
                directoryToFileMoverFiles.put(file.getName(), files);
                moverFiles.addAll(files);
            }
            dirCounter++;
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    directoryProgressBar.setProgress((double) dirCounter / totalCount);
                }
            });
        }
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                directoryProgressBar.setProgress(new Float(-1.0));
                dirCounter = 0;
            }
        });
        return moverFiles;
    }

    public Set<FileMoverFile> createFileMoverFilesListFromDirectory(File directory, final Set<FileMoverFile> files, final Job job) {
        Set<FileMoverFile> selectedDirectoryFiles = new HashSet<>();
        List<File> fileList = new ArrayList<>();
        if (job.isCancelJob()) {
            return selectedDirectoryFiles;
        }
        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY) && isInitialDirectoryAddition) {

            List<String> paths = new ArrayList<>();
            paths.add("BAM\\AGGREGATE");
            paths.add("CRAM");
            paths.add("VCF\\RELEASE\\FILTERED_ON_BAIT");
//            paths.add("VCF\\RELEASE\\FILTERED_ON_BAIT\\TABIX");
            paths.add("REPORTS\\ANNOVAR");
            paths.add("REPORTS\\DEPTH_OF_COVERAGE\\UCSC");
//            paths.add("REPORTS\\GENES_COVERAGE\\UCSC");
//            paths.add("REPORTS\\GENES_COVERAGE\\UCSC_EXONS");
            List<File> projectFiles = new ArrayList<>();
            for (String projectName : projectToSampleMap.keySet()) {
                for (String path : paths) {
                    File newPath = new File(directory.getAbsolutePath() + "\\" + projectName + "\\" + path);
                    if (newPath.listFiles() != null) {
                        projectFiles = Arrays.asList(newPath.listFiles());
                        fileList.addAll(projectFiles);
                    }
                    if (reportTextField.getText().contains(OLD_MENDEL_DESTNIATION_BROSWE_PATH)) {
                        File newPath2 = new File("\\\\isilon-cifs.cidr.jhu.edu\\sequencing\\Seq_Proj" + "\\" + projectName + "\\" + path);
                        projectFiles = Arrays.asList(newPath2.listFiles());
                        fileList.addAll(projectFiles);
                    }
                }
                isInitialDirectoryAddition = false;
            }
        } else if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            CleanUpFileMoverFileAdder cleanUpFileCounter;
            cleanUpFileCounter = new CleanUpFileMoverFileAdder(directory, currentMoveMode, fastqMap);
            return cleanUpFileCounter.getFileMoverFiles();
        } else {
            fileList = Arrays.asList(directory.listFiles());
        }
        for (final File file : fileList) {
            if (job.isCancelJob()) {
                return selectedDirectoryFiles;
            }
            try {
                if (file.isFile()) {
                    for (FileType fileType : currentMoveMode.getFileTypeSet()) {
                        if (file.toString().endsWith(fileType.getExtension())) {

                            if (checkForTypeOverlap(fileType, file.toString())) {

                                if (!currentMoveMode.equals(MoveMode.ALL_FILES)) {
                                    for (String smTag : validSmTags) {
                                        if (file.getName().contains(smTag)) {
                                            FileMoverFile fileMoverFile = new FileMoverFile(file, fileType);
                                            selectedDirectoryFiles.add(fileMoverFile);
                                            files.add(fileMoverFile);
                                            if (!directoryList.contains(fileMoverFile.getDirectory())) {
                                                directoryList.add(fileMoverFile.getDirectory());
                                            }
                                        }
                                    }
                                } else {
                                    FileMoverFile fileMoverFile = new FileMoverFile(file, fileType);
                                    selectedDirectoryFiles.add(fileMoverFile);
                                    files.add(fileMoverFile);
                                    if (!directoryList.contains(fileMoverFile.getDirectory())) {
                                        directoryList.add(fileMoverFile.getDirectory());
                                    }
                                }
                            }
                        }
                    }
                } else if (file.isDirectory()) {
                    createFileMoverFilesListFromDirectory(new File(file.getAbsolutePath()), files, job);
                }
            } catch (Exception ex) {
                System.out.println("NOT ALLOWED IN THERE\n" + ex);
            }
        }
        return files;
    }

    private boolean checkForTypeOverlap(FileType fileType, String file) {
        boolean pass = false;
        if (fileType.equals(FileType.CRAI)) {
            if (!file.toString().endsWith(FileType.CRAM_CRAI.getExtension())) {
                pass = true;
            }
        } else {
            pass = true;
        }
        return pass;
    }

    private boolean isValidMendelFile(FileMoverFile fileMoverFile) {
        MendelFileValidator validator = new MendelFileValidator(fileMoverFile);
        return validator.isValid();
    }

    public void populateComboBox() {
        modeComboBox.getItems().addAll(Arrays.asList(MoveMode.values()));
    }

    private void clearFileTypeCountMap() {
        for (Map.Entry<FileType, Integer> entry : fileTypeCountMap.entrySet()) {
            entry.setValue(0);
        }
    }

    private void setBottomFieldsDisabled(boolean enable) {
        userInputPane.setDisable(enable);
        submitButton.setDisable(enable);
    }

    public void setStage(Stage stage) {

        this.stage = stage;
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                for (Job job : jobHistory) {
                    job.cancelJob();
                }
                applicationRunning = false;
                if (movingOrCopying) {
                    System.out.println("MOVING OR COPYING PLEASE WAIT");
                } else {
                    Platform.exit();
                }
            }
        });
    }

    private void fillCleanUpFolderList() {
        selectedDirectoriesBooleanMap = new HashMap<>();
        directoryProgressBar.setDisable(true);
        dirListView = new ListView<>();
        dirListView.setPrefHeight(directoryPane.getPrefHeight());
        dirListView.setPrefWidth(directoryPane.getPrefWidth());
        directoryPane.getChildren().add(dirListView);
        cleanUpSelectedDirs = new HashSet<>();

        MenuItem selectMenuItem = new MenuItem("Select All");
        MenuItem deselectMenuItem = new MenuItem("Deselect All");
        ContextMenu contextMenu = new ContextMenu(selectMenuItem, deselectMenuItem);
        selectMenuItem.setOnAction(t -> {
            if (!dirListView.isDisabled()) {
                selectAll = true;
                directoryHandler = new DirectoryHandler(allFileMoverFilesList, directoryTreeView, directoryPane, fileTypeGridPane, currentMoveMode, jobQue, fileTypeProgressBar, directoryProgressBar);
                for (String string : dirListView.getItems()) {
                    selectedDirectoriesBooleanMap.get(string).setValue(true);
                }
                if (customCleanUpDir != null) {
                    createNewJob(customCleanUpDir.getAbsolutePath());
                } else {
                    createNewJob(currentMoveMode.getInitialDirectory());
                }
            }
        });

        deselectMenuItem.setOnAction(t -> {
            if (!dirListView.isDisabled()) {
                selectAll = false;
                for (String string : dirListView.getItems()) {
                    selectedDirectoriesBooleanMap.get(string).setValue(false);
                }
            }
        });

        dirListView.setContextMenu(contextMenu);
        dirListView.setCellFactory(CheckBoxListCell.forListView(new Callback<String, ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(final String p) {
                return selectedDirectoriesBooleanMap.get(p);
            }
        }));

        final GuiLoadScreen loadScreen = new GuiLoadScreen(stage);
        loadScreen.show();
        Thread thread = new Thread(() -> Platform.runLater(new Runnable() {
            final List<File> cleanUps = gatherCleanUpFolders();

            @Override
            public void run() {
                loadScreen.hide();

                for (File file : cleanUps) {
                    final String directoryString = file.getName();
                    dirListView.getItems().add(file.getName());
                    final BooleanProperty property = new SimpleBooleanProperty(cleanUpSelectedDirs.contains(directoryString));
                    selectedDirectoriesBooleanMap.put(directoryString, property);
                    property.addListener((ov, t, t1) -> {
                        String directoryFileString = "";
                        if (customCleanUpDir != null) {
                            directoryFileString = customCleanUpDir.getAbsolutePath() + "\\" + directoryString;
                        } else {
                            directoryFileString = currentMoveMode.getInitialDirectory() + "\\" + directoryString;
                        }
                        if (t1) {
                            cleanUpSelectedDirs.add(directoryString);
                            directoryProgressBar.setOpacity(1);
                            dirListView.setDisable(true);
                            masterKeyBrowseButton.setDisable(true);
                            directoryProgressBar.setVisible(true);
                            masterKeyTextField.setDisable(true);
//                                        familiesPrefixTextField.setDisable(true);

                            if (directoryToFileMoverFiles.containsKey(directoryString)) {
                                if (allFileMoverFilesList.addAll(directoryToFileMoverFiles.get(directoryString))) {
                                    createFileTypeBoxes();
                                }
                            }
                            if (cleanUpStrings.contains(directoryString) && property.getValue() == true && selectAll == false) {
                                selectedDir = directoryString;
                                directoryHandler = new DirectoryHandler(allFileMoverFilesList, directoryTreeView, directoryPane, fileTypeGridPane, currentMoveMode, jobQue, fileTypeProgressBar, directoryProgressBar);
                                createNewJob(directoryFileString);
                            }
                        } else {
                            cleanUpSelectedDirs.remove(directoryString);
                            for (Job job : jobHistory) {
                                if (job.getDirectory().equals(directoryFileString)) {
                                    job.cancelJob();
                                }
                            }
                            if (directoryToFileMoverFiles.containsKey(directoryString)) {
                                if (allFileMoverFilesList.removeAll(directoryToFileMoverFiles.get(directoryString))) {
                                    createFileTypeBoxes();
                                }
                            }
                        }
                    });
                }
            }
        }));
        thread.start();
    }

    private List<File> gatherCleanUpFolders() {
        CleanUpFolderGatherer gatherer = new CleanUpFolderGatherer(currentMoveMode, cleanUpStrings);
        gatherer.setCustomBclDir(customCleanUpDir);
        List<File> cleanUpFiles = gatherer.getCleanUpDirectoryList();
        cleanUpStrings = gatherer.getCleanUpStrings();
        return cleanUpFiles;
    }

    private List<File> getSelectedDirectories() {
        List<File> selectedDirList = new ArrayList<>();
        for (String string : dirListView.getItems()) {
            if (customCleanUpDir != null) {
//            if (MoveMode.CUSTOM_BCL_CLEAN_UP.equals(currentMoveMode)) {
                selectedDirList.add(new File(customCleanUpDir.getAbsolutePath() + "\\" + string));
            } else {
                selectedDirList.add(new File(currentMoveMode.getInitialDirectory() + "\\" + string));
            }
        }
        return selectedDirList;
    }

    private void resetFileMover() {
        currentMoveMode = modeComboBox.getValue();
        fileTypeGridPane.getChildren().clear();
        allFileMoverFilesList = new HashSet<>();
        selectedDirectoriesBooleanMap = new HashMap<>();
        cleanUpSelectedDirs = new HashSet<>();
        directoryToFileMoverFiles = new HashMap<>();
        cleanUpStrings = new ArrayList<>();
        directoryList = new ArrayList<>();
        selectAll = false;
        selectAllUsed = false;
        selectedDir = null;
        isInitialDirectoryAddition = true;
        jobHistory.clear();
        jobQue.clear();
        destinationTextField.clear();
        masterKeyTextField.clear();
        fileLocationTextField.setText("");
        fileLocationLabel.setText("");
        reportTextField.clear();
        fileTypeCountMap = new HashMap<>();
    }

}
