 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import java.util.EnumSet;
import java.util.Set;

/**
 *
 * @author kduerr1
 */
public enum FileType {
    ANY("", false),
    BAM(".bam", false),
    BAI(".bai", false),
    TXT("_ANNOVAR_REPORT.txt", false),
    VCF(".vcf", false),
    MS_ONBAIT_VCF("MS_OnBait.vcf.gz", false),
//    MS_ONBAIT_VCF("MS_OnBait.vcf", false),
    TBI(".tbi", false),
    GZ(".gz", false),
    CSV(".csv", false),
    GENE_SUMMARY_CSV("gene_summary.csv", false),
    INTERVAL_SUMMARY_CSV("interval_summary.csv", false),
    DICTIONARY_CSV("DICTIONARY_2013_06_13.csv", false),
    DICTIONARY2_CSV("DICTIONARY_2016_02_01.csv", false),
    PDF(".pdf", false),
    IDX(".vcf.gz.tbi", false),
    MS_ONBAIT_VCF_IDX("MS_OnBait.vcf.gz.tbi", false),
    FASTQ(".fastq.gz", false),
//    IDX(".vcf.idx", false),
//    MS_ONBAIT_VCF_IDX("MS_OnBait.vcf.idx", false),
    //
    CONFIG("Config", true),
    THUMBNAIL_IMAGES("Thumbnail_Images", true),
    IMAGES("Images", true),
    QUEUED("Queued", true),
    RECIPE("Recipe", true),
    PERIODICSAVERATES("PeriodicSaveRates", true),
    DIAG("Diag", true),
    LOGS("Logs", true),
    DATA_TILESTATUS("Data/TileStatus", true),
    DATA_INTENSITIES("Data/Intensities", true),
    //
    DATA("Data", true),
    //Added 10/29/18
    CRAM(".cram", false),
    CRAI(".crai", false),
    CRAM_CRAI(".cram.crai", false),
    //
    DATA_INTENSITIES_BASECALLS("Data/Intensities/BaseCalls", true);

    private final String extension;
    private boolean isDir;

    private FileType(String extension, Boolean isDir) {
        this.extension = extension;
        this.isDir = isDir;
    }

    public String getExtension() {
        return extension;
    }

    public boolean isDir() {
        return isDir;
    }
    
    public static final Set<FileType> ALL_FILES_FILE_TYPES = EnumSet.of(BAM, BAI, TXT, VCF, TBI, GZ, CRAM, CRAM_CRAI, CRAI);
    public static final Set<FileType> QC_REPORT_FILE_TYPES = EnumSet.of(CSV, PDF, BAM, BAI, TXT, VCF, TBI, IDX, GZ, CRAM, CRAM_CRAI, CRAI);
    public static final Set<FileType> QC_REPORT_FAMILY_FILE_TYPES = EnumSet.of(GENE_SUMMARY_CSV, INTERVAL_SUMMARY_CSV, DICTIONARY_CSV,DICTIONARY2_CSV, BAM, BAI, TXT, MS_ONBAIT_VCF, MS_ONBAIT_VCF_IDX, CRAM, CRAM_CRAI, CRAI);
//    public static final Set<FileType> QC_REPORT_FAMILY_FILE_TYPES = EnumSet.of(GENE_SUMMARY_CSV, INTERVAL_SUMMARY_CSV, DICTIONARY_CSV, BAM, BAI, TXT, TBI, GZ, MS_ONBAIT_VCF, MS_ONBAIT_VCF_IDX);
    public static final Set<FileType> SEQ_RUN_CLEAN_UP_FILE_TYPES = EnumSet.of(THUMBNAIL_IMAGES, CONFIG, PERIODICSAVERATES, DIAG, LOGS, DATA_TILESTATUS);
    public static final Set<FileType> NOVA_CLEAN_UP_FILE_TYPES = EnumSet.of(THUMBNAIL_IMAGES, CONFIG, LOGS, DATA);
    public static final Set<FileType> MISEQ_RUN_CLEAN_UP_FILE_TYPES = EnumSet.of(IMAGES, THUMBNAIL_IMAGES, LOGS,RECIPE, CONFIG, DATA_INTENSITIES_BASECALLS);
    public static final Set<FileType> BCL_CLEAN_UP_FILE_TYPES = EnumSet.of(DATA);
    public static final Set<FileType> BAD_FLOW_CELL_CLEAN_UP_FILE_TYPES = EnumSet.of(CONFIG, THUMBNAIL_IMAGES, PERIODICSAVERATES, DIAG, LOGS, DATA);
    public static final Set<FileType> NOVA_BAD_FLOW_CELL_CLEAN_UP_FILE_TYPES = EnumSet.of(CONFIG, THUMBNAIL_IMAGES, LOGS, DATA);
    public static final Set<FileType> FASTQ_CLEAN_UP_FILE_TYPES = EnumSet.of(FASTQ);


}
