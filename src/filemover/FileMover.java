/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author kduerr1
 */
public class FileMover extends Application {

//    private static final double STAGE_HEIGHT = 690;
//    private static final double STAGE_WIDTH = 960;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource("FileMoverv2.fxml"));
        Parent root = (Parent) fXMLLoader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/edu/jhmi/cidr/lib/conveniencestore/guiutils/style/style.css"); 
        final FileMoverController controller = fXMLLoader.getController();

        controller.setStage(stage);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle("Sequencing Report File Mover");
        stage.show();

//        stage.setHeight(STAGE_HEIGHT);
//        stage.setWidth(STAGE_WIDTH);

        scene.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent ev) {
                if (controller.shutdown()) {
                    System.exit(0);
                    Platform.exit();
                    ev.consume();
                } else {
                    ev.consume();
                }
            }
        });
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
