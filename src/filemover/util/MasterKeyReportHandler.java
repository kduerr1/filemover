/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiDialog;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author kduerr1
 */
public class MasterKeyReportHandler {

    private final TextField masterKeyTextField;
    private final Stage stage;
    private final List<String> columns = Arrays.asList(
            "Project Name",
            "SM_Tag",
            "Subject_ID");
    private File masterKeyReport;
    private ParseOmaticStrategy<String> parseOmatic;
    private Map<String, List<String>> familyToSampleMap;
    private Map<String, List<Integer>> familyToSampleIntMap;
    private Map<String, List<String>> projectToFamilyMap;
    private Map<String, List<Integer>> projectToFamilyIntMap;
    private List<String> smTags;
    private List<String> missingColumns;

    private int smTagCount;

    public MasterKeyReportHandler(TextField masterKeyTextField, Stage stage) {
        this.masterKeyTextField = masterKeyTextField;
        this.stage = stage;
    }

    public void parseMasterKeyReport() throws Exception {
        smTags = new ArrayList<>();
        masterKeyReport = new File(masterKeyTextField.getText());
        familyToSampleMap = new HashMap<>();
        familyToSampleIntMap = new HashMap<>();
        projectToFamilyMap = new HashMap<>();
        projectToFamilyIntMap = new HashMap<>();
        missingColumns = new ArrayList<>();
        if (masterKeyReport.isFile()) {
            boolean missingColumn = false;
            parseOmatic = ParseOmatic.getStringColumnParser(masterKeyReport, 1, ",", columns, columns);
            parseOmatic.parse();
            Map<String, Map<Integer, String>> columnMap = parseOmatic.getParsedContent().columnMap();
            missingColumns = parseOmatic.getMissingRequiredColumns();
            if (missingColumns.isEmpty()) {
                for (Entry<Integer, String> entry : columnMap.get("Subject_ID").entrySet()) {
                    if (!entry.getValue().startsWith("NA")
                            && !entry.getValue().startsWith("CIDR")
                            && !familyToSampleMap.containsKey(entry.getValue())
                            && !entry.getValue().contains("_")
                            && entry.getValue().contains(".")) {
                        familyToSampleMap.put(entry.getValue().substring(0, entry.getValue().indexOf(".")), new ArrayList<String>());
                        familyToSampleIntMap.put(entry.getValue().substring(0, entry.getValue().indexOf(".")), new ArrayList<Integer>());

                    } else if (!entry.getValue().startsWith("NA")
                            && !familyToSampleMap.containsKey(entry.getValue().substring(0, entry.getValue().indexOf("_")))
                            && !entry.getValue().startsWith("CIDR")) {
                        familyToSampleMap.put(entry.getValue().substring(0, entry.getValue().indexOf("_")), new ArrayList<String>());
                        familyToSampleIntMap.put(entry.getValue().substring(0, entry.getValue().indexOf("_")), new ArrayList<Integer>());
                    }
                }

                for (Entry<Integer, String> entry : columnMap.get("Subject_ID").entrySet()) {
                    if (!entry.getValue().startsWith("NA")
                            && !entry.getValue().startsWith("CIDR")
                            && entry.getValue().contains(".")
                            //                            && !familyToSampleMap.containsKey(entry.getValue())
                            && !entry.getValue().contains("_")) {
                        List<Integer> list = familyToSampleIntMap.get(entry.getValue().substring(0, entry.getValue().indexOf(".")));
                        list.add(entry.getKey());
                        familyToSampleIntMap.put(entry.getValue().substring(0, entry.getValue().indexOf(".")), list);

                    } else if (!entry.getValue().startsWith("NA")
                            && familyToSampleMap.containsKey(entry.getValue().substring(0, entry.getValue().indexOf("_")))) {
                        List<Integer> list = familyToSampleIntMap.get(entry.getValue().substring(0, entry.getValue().indexOf("_")));
                        list.add(entry.getKey());
                        familyToSampleIntMap.put(entry.getValue().substring(0, entry.getValue().indexOf("_")), list);
                    }
                }

                for (Map.Entry<Integer, String> entry : columnMap.get("Project Name").entrySet()) {
                    if (!projectToFamilyMap.containsKey(entry.getValue())) {
                        projectToFamilyMap.put(entry.getValue(), new ArrayList<String>());
                        projectToFamilyIntMap.put(entry.getValue(), new ArrayList<Integer>());
                    }
                }

                for (Map.Entry<Integer, String> entry : columnMap.get("Project Name").entrySet()) {
                    if (projectToFamilyMap.containsKey(entry.getValue())) {
                        List<Integer> list = projectToFamilyIntMap.get(entry.getValue());
                        list.add(entry.getKey());
                        projectToFamilyIntMap.put(entry.getValue(), list);
                    }
                }

                for (Entry<Integer, String> entry : columnMap.get("SM_Tag").entrySet()) {
                    smTags.add(entry.getValue());
                    for (Entry<String, List<Integer>> entry1 : familyToSampleIntMap.entrySet()) {
                        String string = entry1.getKey();
                        List<Integer> list = entry1.getValue();
                        if (list.contains(entry.getKey())) {
                            List<String> list2 = familyToSampleMap.get(string);
                            list2.add(entry.getValue());
                            familyToSampleMap.put(string, list2);
                        }
                    }
                }
                for (Entry<Integer, String> entry : columnMap.get("Subject_ID").entrySet()) {
                    for (Entry<String, List<Integer>> entry1 : projectToFamilyIntMap.entrySet()) {
                        String string = entry1.getKey();
                        List<Integer> list = entry1.getValue();
                        if (list.contains(entry.getKey())) {
                            Set<String> list2 = new HashSet<>(projectToFamilyMap.get(string));
                            if (!entry.getValue().startsWith("NA")
                                    && entry.getValue().contains(".")) {
                                list2.add(entry.getValue().substring(0, entry.getValue().indexOf(".")));
                                projectToFamilyMap.put(string, new ArrayList<>(list2));
                            } else if (!entry.getValue().startsWith("NA")
                                    && !list2.contains(entry.getValue().substring(0, entry.getValue().indexOf("_")))) {
                                list2.add(entry.getValue().substring(0, entry.getValue().indexOf("_")));
                                projectToFamilyMap.put(string, new ArrayList<>(list2));
                            }
                        }
                    }
                }
            } else {
                missingColumn = true;
            }

            if (missingColumn) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);

                alert.setContentText("Selected Master Key Report does not contain correct columns");
                alert.setHeaderText("Master Key Report Error");
                alert.setTitle("Master Key Report Error");
                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                                       masterKeyTextField.clear();
                    }

                });

//                GuiDialog guiDialog = new GuiDialog(stage);
//                guiDialog.showDialog(Arrays.asList("Selected Master Key Report does not contain correct columns"), GuiDialog.GuiDialogButton.OK);
//                masterKeyTextField.clear();
            } else {
                smTagCount = columnMap.get("SM_Tag").size();
            }
        }
    }

    public int getSmTagCount() {
        return smTagCount;
    }

    public Map<String, List<String>> getFamilyToSampleMap() {
        return familyToSampleMap;
    }

    public Map<String, List<String>> getProjectToFamilyMap() {
        return projectToFamilyMap;
    }

    public List<String> getSmTags() {
        return smTags;
    }
}
