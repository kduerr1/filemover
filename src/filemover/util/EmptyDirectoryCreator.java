/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author kduerr1
 */
public class EmptyDirectoryCreator {

//    private final Map<String, List<String>> projectToFamilyMap;
//    private final TextField familiesPrefixTextField;
    private final File selectedDestination;
    private final File phenoDBDir;

    public EmptyDirectoryCreator(File selectedDestination) {
//        this.projectToFamilyMap = projectToFamilyMap;
//        this.familiesPrefixTextField = familiesPrefixTextField;
        this.selectedDestination = selectedDestination;
        String date = new SimpleDateFormat("MMddyy").format(new Date());
        this.phenoDBDir = new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "PhenoDB_Upload_" + date);
    }

    public void createDirectories() {

        //TODO : PhenoDB_Upload_DDMMYY (date of copy)
        List<File> emptyMendelDirectories = new ArrayList<>();
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "Analysis_Pipeline_Details"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "Analysis_Pipeline_Files"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "BED_files"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "MultiSampleVCF"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "QC_Reports"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "Sample_Info"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "dataset_to_PI" + "\\" + "Variant_Summary_Stats"));
        emptyMendelDirectories.add(phenoDBDir);

        for (File file : emptyMendelDirectories) {
            file.mkdirs();
        }

//        for (List<String> families : projectToFamilyMap.values()) {
//            for (String family : families) {
//                if (!family.equals("CIDR")) {
//
//            
//
//                    String commonDirectory = selectedDestination.toString() + "\\" + "dataset_to_PI"
//                            + "\\" + familiesPrefixTextField.getText() + "_Fam_" + family + "\\";
////                    String commonDirectory = selectedDestination.toString() + "\\" + "dataset_to_PI"
////                            + "\\" + familiesPrefixTextField.getText() + "_Fam_" + family + "\\";
//                    File sampleInfoDirectory = new File(commonDirectory + "Sample_Info");
//                    sampleInfoDirectory.mkdirs();
//                    File annovarInfoDirectory = new File(commonDirectory + "ANNOVAR");
//                    annovarInfoDirectory.mkdirs();
//                    File geneDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
//                    geneDirectory.mkdirs();
//                    File vcfDirectory = new File(commonDirectory + "VCF");
//                    vcfDirectory.mkdirs();
//                    File vcfFilterDirectory = new File(commonDirectory + "VCF\\FILTERED_ON_BAIT");
//                    vcfFilterDirectory.mkdirs();
////                    File vcfFilterTabDirectory = new File(commonDirectory + "VCF\\FILTERED_ON_BAIT\\TABIX");
////                    vcfFilterTabDirectory.mkdirs();
//                    File bamDirectory = new File(commonDirectory + "BAM");
//                    bamDirectory.mkdirs();
//                } else {
//                    String commonDirectory = selectedDestination.toString() + "\\" + "dataset_to_PI"
//                            + "\\" + "M_Valle" + "_Fam_" + "HapMapControls" + "\\";
////                    String commonDirectory = selectedDestination.toString() + "\\" + "dataset_to_PI"
////                            + "\\" + familiesPrefixTextField.getText() + "_Fam_" + "HapMapControls" + "\\";
//                    File sampleInfoDirectory = new File(commonDirectory + "Sample_Info");
//                    sampleInfoDirectory.mkdirs();
//                    File annovarInfoDirectory = new File(commonDirectory + "ANNOVAR");
//                    annovarInfoDirectory.mkdirs();
//                    File geneDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
//                    geneDirectory.mkdirs();
//                    File vcfDirectory = new File(commonDirectory + "VCF");
//                    vcfDirectory.mkdirs();
//                    File vcfFilterDirectory = new File(commonDirectory + "VCF\\FILTERED_ON_BAIT");
//                    vcfFilterDirectory.mkdirs();
////                    File vcfFilterTabDirectory = new File(commonDirectory + "VCF\\FILTERED_ON_BAIT\\TABIX");
////                    vcfFilterTabDirectory.mkdirs();
//                    File bamDirectory = new File(commonDirectory + "BAM");
//                    bamDirectory.mkdirs();
//                }
//            }
//        }
    }

    public File getPhenoDBDir() {
        return phenoDBDir;
    }
}
