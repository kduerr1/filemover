/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.MoveMode;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

/**
 *
 * @author kduerr1
 */
public class UserInputValidator {

    private final List<String> errorList = new ArrayList<>();
    private final MoveMode currentMoveMode;
    private final ProgressBar fileTypeProgressBar;
    private final boolean copy;
    private final boolean move;
    private final TextField reportTextField;
    private final TextField fileLocationTextField;
//    private final TextField familiesPrefixTextField;
    private final TextField destinationTextField;
    private final TextField masterKeyTextField;
    private final HashSet<String> cleanUpSelectedDirs;

    public UserInputValidator(MoveMode currentMoveMode, ProgressBar fileTypeProgressBar,
            boolean copy, boolean move, TextField reportTextField, TextField fileLocationTextField,
             TextField destinationTextField,
            TextField masterKeyTextField, HashSet<String> cleanUpSelectedDirs) {

        this.currentMoveMode = currentMoveMode;
        this.fileTypeProgressBar = fileTypeProgressBar;
        this.copy = copy;
        this.move = move;
        this.reportTextField = reportTextField;
        this.fileLocationTextField = fileLocationTextField;
//        this.familiesPrefixTextField = familiesPrefixTextField;
        this.destinationTextField = destinationTextField;
        this.masterKeyTextField = masterKeyTextField;
        this.cleanUpSelectedDirs = cleanUpSelectedDirs;
    }

    public boolean isValid() {
        if (fileTypeProgressBar.isVisible()) {
            errorList.add("Please wait while files are beging counted");
        }
        
        if (!copy
                && !move
                && !currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)
                && !MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            errorList.add("Select move or copy.");
        }
        
        if (!currentMoveMode.equals(MoveMode.ALL_FILES)
                && reportTextField.getText().isEmpty()
                && !MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            errorList.add("Select a QC Report.");
        }
        
        if (fileLocationTextField.getText().isEmpty()
                && !MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            errorList.add("Select a file location.");
        } else if (!new File(fileLocationTextField.getText()).isDirectory()
                && !MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            errorList.add("File location selected is not a directory.");
        }
        
        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
            if (masterKeyTextField.getText().isEmpty()) {
                errorList.add("Select a Master Key Report.");
            }
//            if (familiesPrefixTextField.getText().isEmpty() || !isValidName(familiesPrefixTextField.getText())) {
//                errorList.add("Enter a valid prefix for the Families folders.");
//            }
        }

        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            if (cleanUpSelectedDirs.isEmpty()) {
                errorList.add("No selected directories");
            }
        }

        if (destinationTextField.getText().isEmpty()) {
            errorList.add("Select a destination.");
        } else if (!new File(destinationTextField.getText()).isDirectory()) {
            errorList.add("Destination selected is not a directory.");
        }
        
        if (errorList.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidName(String text) {
        Pattern pattern = Pattern.compile(
                "# Match a valid Windows filename (unspecified file system).          \n"
                + "^                                # Anchor to start of string.        \n"
                + "(?!                              # Assert filename is not: CON, PRN, \n"
                + "  (?:                            # AUX, NUL, COM1, COM2, COM3, COM4, \n"
                + "    CON|PRN|AUX|NUL|             # COM5, COM6, COM7, COM8, COM9,     \n"
                + "    COM[1-9]|LPT[1-9]            # LPT1, LPT2, LPT3, LPT4, LPT5,     \n"
                + "  )                              # LPT6, LPT7, LPT8, and LPT9...     \n"
                + "  (?:\\.[^.]*)?                  # followed by optional extension    \n"
                + "  $                              # and end of string                 \n"
                + ")                                # End negative lookahead assertion. \n"
                + "[^<>:\"/\\\\|?*\\x00-\\x1F]*     # Zero or more valid filename chars.\n"
                + "[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]  # Last char is not a space or dot.  \n"
                + "$                                # Anchor to end of string.            ",
                Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.COMMENTS);
        Matcher matcher = pattern.matcher(text);
        boolean isMatch = matcher.matches();
        return isMatch;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
