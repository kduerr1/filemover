/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;
import filemover.FileMoverFile;
import filemover.FileType;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author kduerr1
 */
public class FamilyDirectoryFileSorter {

    private String familiesPrefix;
    private final Map<String, List<String>> familyToSampleMap;
    private final File selectedDestination;
    private File newDestination = null;
    private FileMoverFile fileMoverFile;
    private final Map<String, List<String>> projectToFamilyMap;
    private Map<FileType, Integer> fileTypeCountMap = new HashMap<>();

    public FamilyDirectoryFileSorter(Map<String, List<String>> familyToSampleMap,
            Map<String, List<String>> projectToFamilyMap,
            File selectedDestination, Map<FileType, Integer> fileTypeCountMap) {
        this.familyToSampleMap = familyToSampleMap;
        this.selectedDestination = selectedDestination;
        this.projectToFamilyMap = projectToFamilyMap;
        this.fileTypeCountMap = fileTypeCountMap;
    }

    public void translateFilemoverFile(FileMoverFile fileMoverFile) {
        this.fileMoverFile = fileMoverFile;
        String sampleFamily = "HapMapControls";

        familiesPrefix = "M_Valle";

        String commonDirectory = selectedDestination.toString() + "\\" + "dataset_to_PI"
                + "\\" + familiesPrefix + "_Fam_" + "HapMapControls" + "\\";

        boolean createBamDir = false;

        if (fileTypeCountMap.get(FileType.BAM) > 0) {
            createBamDir = true;
        }

        createEmptyDirs(commonDirectory, createBamDir);

        for (Map.Entry<String, List<String>> entry : familyToSampleMap.entrySet()) {
            String familyString = entry.getKey();
            List<String> list = entry.getValue();
            for (String smTagString : list) {
                if (fileMoverFile.getFile().getName().startsWith(smTagString)) {
                    sampleFamily = familyString;

                    for (Map.Entry<String, List<String>> entry1 : projectToFamilyMap.entrySet()) {
                        String key = entry1.getKey();
                        List<String> value = entry1.getValue();
                        if (value.contains(sampleFamily)) {
                            familiesPrefix = familiesPrefix + "_" + key.substring(key.lastIndexOf("_") + 1, key.length());

                            String prefix = selectedDestination.toString() + "\\"
                                    + "dataset_to_PI" + "\\"
                                    + familiesPrefix + "_Fam_" + sampleFamily + "\\";
                            createEmptyDirs(prefix, createBamDir);
                        }
                    }
                }
            }
        }

        String commonPrefix = selectedDestination.toString() + "\\"
                + "dataset_to_PI" + "\\"
                + familiesPrefix + "_Fam_" + sampleFamily + "\\";

        if (fileMoverFile.getDirectory().toString().endsWith("BAM\\AGGREGATE")) {
            newDestination = new File(
                    commonPrefix
                    + "BAM");
        } else if (fileMoverFile.getDirectory().toString().endsWith("CRAM")) {
            newDestination = new File(
                    commonPrefix
                    + "CRAM");
        } else if (fileMoverFile.getDirectory().toString().endsWith("VCF\\RELEASE\\FILTERED_ON_BAIT")) {
            newDestination = new File(
                    commonPrefix
                    + "VCF" + "\\"
                    + "filtered_on_bait");
            fileMoverFile.setIsPhenoDBFile(true);
        } else if (fileMoverFile.getDirectory().toString().endsWith("VCF\\RELEASE\\FILTERED_ON_BAIT\\TABIX")) {
            newDestination = new File(
                    commonPrefix
                    + "VCF" + "\\"
                    + "filtered_on_bait" + "\\"
                    + "TABIX");
        } else if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\ANNOVAR")) {
            newDestination = new File(
                    commonPrefix
                    + "ANNOVAR");
            fileMoverFile.setIsPhenoDBFile(true);
        } else if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\DEPTH_OF_COVERAGE\\UCSC")) {
            newDestination = new File(
                    commonPrefix
                    + "GENE_EXONIC_CVG");
        }
    }

    public File getNewDestination() {
        return newDestination;
    }

    public File getNewPath() {
        if (newDestination == null) {
            return null;
        }
        return new File(newDestination + "\\" + fileMoverFile.getFile().getName());
    }

    public File getNewDirectory() {
        if (newDestination == null) {
            return null;
        }
        return new File(newDestination.getAbsolutePath());
    }

    private void createEmptyDirs(String commonDirectory, boolean createBam) {
        File sampleInfoDirectory = new File(commonDirectory + "Sample_Info");
        sampleInfoDirectory.mkdirs();
        File annovarInfoDirectory = new File(commonDirectory + "ANNOVAR");
        annovarInfoDirectory.mkdirs();
        File geneDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
        geneDirectory.mkdirs();
        File vcfDirectory = new File(commonDirectory + "VCF");
        vcfDirectory.mkdirs();
        if (createBam) {
            File bamDirectory = new File(commonDirectory + "BAM");
            bamDirectory.mkdirs();
        }
    }
}
