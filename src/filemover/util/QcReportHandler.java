/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiDialog;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author kduerr1
 */
public class QcReportHandler {

    private final TextField qcReportTextField;
    private final Stage stage;
    private final List<String> columns = Arrays.asList("SM_TAG", "PROJECT");
    private final File qcReport;
    private ParseOmaticStrategy<String> parseOmatic;
    private int smTagCount;
    private final List<String> qcReportSmTags;
    private final Set<String> projects;
    private boolean missingSmTags;
    private Map<String, List<String>> projectToSampleMap;
    private Map<String, List<Integer>> projectToSampleIntMap;

    public QcReportHandler(TextField qcReportTextField, Stage stage) {
        this.qcReportTextField = qcReportTextField;
        this.stage = stage;
        this.qcReport = new File(qcReportTextField.getText());
        this.qcReportSmTags = new ArrayList<>();
        missingSmTags = false;
        this.projects = new HashSet<>();
    }

    public void parseQcReport() throws Exception {
        projectToSampleMap = new HashMap<>();
        projectToSampleIntMap = new HashMap<>();
        if (qcReport.isFile()) {
            parseOmatic = ParseOmatic.getStringColumnParser(qcReport, 1, ",", columns, columns);
            try {
                parseOmatic.parse();
            } catch (Exception e) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);

                alert.setContentText("ERROR: Parsing report");
                alert.setHeaderText("QC Parsing Error");
                alert.setTitle("Error");
                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                        qcReportTextField.clear();
                        return;
                    }
                });

//                GuiDialog guiDialog = new GuiDialog(stage);
//                guiDialog.showDialog("ERROR: Parsing report", GuiDialog.GuiDialogButton.OK);
//                qcReportTextField.clear();
//                return;
            }
            Map<String, Map<Integer, String>> columnMap = parseOmatic.getParsedContent().columnMap();
            for (Map.Entry<Integer, String> entry : columnMap.get("PROJECT").entrySet()) {
                projects.add(entry.getValue());
                if (!projectToSampleMap.containsKey(entry.getValue())) {
                    projectToSampleMap.put(entry.getValue(), new ArrayList<String>());
                    projectToSampleIntMap.put(entry.getValue(), new ArrayList<Integer>());
                }
            }

            for (Map.Entry<Integer, String> entry : columnMap.get("PROJECT").entrySet()) {
                if (projectToSampleMap.containsKey(entry.getValue())) {
                    List<Integer> list = projectToSampleIntMap.get(entry.getValue());
                    list.add(entry.getKey());
                    projectToSampleIntMap.put(entry.getValue(), list);
                }
            }

            for (Map.Entry<Integer, String> entry : columnMap.get("SM_TAG").entrySet()) {
                for (Map.Entry<String, List<Integer>> entry1 : projectToSampleIntMap.entrySet()) {
                    String string = entry1.getKey();
                    List<Integer> list = entry1.getValue();
                    if (list.contains(entry.getKey())) {
                        List<String> list2 = projectToSampleMap.get(string);
                        list2.add(entry.getValue());
                        projectToSampleMap.put(string, list2);
                    }
                }
                qcReportSmTags.add(entry.getValue());
                if (entry.getValue() == null) {
                    missingSmTags = true;
                }
            }
            if (missingSmTags) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);

                alert.setContentText("Please select another QC Report");
                alert.setHeaderText("Selected QC Report is missing data");
                alert.setTitle("QC Report Error");
                alert.showAndWait().ifPresent(response -> {
                    if (response == ButtonType.OK) {
                        qcReportTextField.clear();
                    }
                });

//                GuiDialog guiDialog = new GuiDialog(stage);
//                guiDialog.showDialog(Arrays.asList("Selected QC Report is missing data", "Please select another QC Report."), GuiDialog.GuiDialogButton.OK);
//                qcReportTextField.clear();
            } else {
                smTagCount = columnMap.get("SM_TAG").size();
            }
        }
    }

    public int getSmTagCount() {
        return smTagCount;
    }

    public List<String> getQcReportSmTags() {
        return qcReportSmTags;
    }

    public boolean isMissingSmTags() {
        return missingSmTags;
    }

    public Map<String, List<String>> getProjectToSampleMap() {
        return projectToSampleMap;
    }

    public Set<String> getProjects() {
        return projects;
    }

}
