/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author kduerr1
 */
public class Logger implements Runnable {

    private final Queue<String> queue = new PriorityQueue<>();
    private final File logFile;

    public Logger(File logFile) {
        this.logFile = logFile;
    }

    @Override
    public void run() {
        purgeLogQueue();
    }

    public void addToLog(String string) {
        queue.add(string);
    }

    public void purgeLogQueue() {
        try (BufferedWriter out = new BufferedWriter(new BufferedWriter(new FileWriter(logFile, true)))) {
            while (queue.peek() != null) {
                String line = queue.poll().toString();
                out.write(line + "\n");
            }
        } catch (Exception e) {
//            e.printStackTrace();
            System.err.println("Exception in log file. " + e.getMessage());
        }
    }
}
