/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author kduerr1
 */
public class ParentLogger {

    private static final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
    private static final Map<File, Logger> folderToLoggerMap = new HashMap<>();
    private static final Map<String, String> dirToLogMap = new HashMap<>();

    private ParentLogger() {
    }

    public static void start(List<File> folders) {
        for (File folder : folders) {
            Date date = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyMMdd_kkmm");
            String outFileName = "Moved_" + dateFormat.format(date) + ".csv";
            dirToLogMap.put(folder.getName(), outFileName);
            File folderLog = new File(folder, outFileName);
            if (!folderLog.getParentFile().exists()) {
                folderLog.getParentFile().mkdirs();
            }
            Logger folderLogger = new Logger(folderLog);
            folderToLoggerMap.put(folder, folderLogger);
            executorService.scheduleAtFixedRate(folderLogger, 0, 5000, TimeUnit.MILLISECONDS);
        }
    }

    public static void stopLogger() {
        for (Logger logger : folderToLoggerMap.values()) {
            logger.purgeLogQueue();
        }
        executorService.shutdown();
    }

    public static void addToLog(String fileMovedMessage, File folder) {
        Logger folderLogger = folderToLoggerMap.get(folder);
        folderLogger.addToLog(fileMovedMessage);
    }

    public static Map<String, String> getDirToLogMap() {
        return dirToLogMap;
    }

}
