/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.scene.control.TextField;

/**
 *
 * @author kduerr1
 */
public class DirectoryCreator {

    private final Map<String, List<String>> projectToFamilyMap;
    private final TextField familiesPrefixTextField;
    private final File selectedDestination;

    public DirectoryCreator(Map<String, List<String>> projectToFamilyMap, TextField familiesPrefixTextField, File selectedDestination) {
        this.projectToFamilyMap = projectToFamilyMap;
        this.familiesPrefixTextField = familiesPrefixTextField;
        this.selectedDestination = selectedDestination;
    }

    public void createDirectories() {
        List<File> emptyMendelDirectories = new ArrayList<>();
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "Analysis_Pipeline_Details"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "Analysis_Pipeline_Files"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "BED_files"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "MultiSampleVCF"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "QC_Reports"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "Sample_Info"));
        emptyMendelDirectories.add(new File(selectedDestination.toString() + "\\" + "rawdataset_to_PI" + "\\" + "Variant_Summary_Stats"));

        for (File file : emptyMendelDirectories) {
            file.mkdirs();
        }

        for (List<String> families : projectToFamilyMap.values()) {
            for (String family : families) {
                if (!family.equals("CIDR")) {
                    String commonDirectory = selectedDestination.toString() + "\\" + "rawdataset_to_PI"
                            + "\\" + familiesPrefixTextField.getText() + "_Fam_" + family + "\\";
                    File sampleInfoDirectory = new File(commonDirectory + "Sample_Info");
                    sampleInfoDirectory.mkdirs();
                    File annovarInfoDirectory = new File(commonDirectory + "ANNOVAR");
                    annovarInfoDirectory.mkdirs();
                    File geneDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
                    geneDirectory.mkdirs();
                    File vcfDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
                    vcfDirectory.mkdirs();
                    File bamDirectory = new File(commonDirectory + "BAM");
                    bamDirectory.mkdirs();
                } else {
                    String commonDirectory = selectedDestination.toString() + "\\" + "rawdataset_to_PI"
                            + "\\" + familiesPrefixTextField.getText() + "_Fam_" + "HapMapControls" + "\\";
                    File sampleInfoDirectory = new File(commonDirectory + "Sample_Info");
                    sampleInfoDirectory.mkdirs();
                    File annovarInfoDirectory = new File(commonDirectory + "ANNOVAR");
                    annovarInfoDirectory.mkdirs();
                    File geneDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
                    geneDirectory.mkdirs();
                    File vcfDirectory = new File(commonDirectory + "GENE_EXONIC_CVG");
                    vcfDirectory.mkdirs();
                    File bamDirectory = new File(commonDirectory + "BAM");
                    bamDirectory.mkdirs();
                }
            }
        }
    }
}
