/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.MoveMode;
import java.io.File;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author kduerr1
 */
public class FileMoverModeSetup {

    MoveMode currentMoveMode;
    File customCleanUpDir = null;
    Pane directoryPane;
    Pane ModePane;
    Pane userInputPane;
    Label destinationIntroLabel;
    Label masterKeyLabel;
    Label familiesPrefixLabel;
    Label directoryLabel;
    Label percentProgressLabel;
    Label currentProgressFileLabel;
    Label fileLocationLabel;
    TextField masterKeyTextField;
    TextField familiesPrefixTextField;
    TextField reportTextField;
    TextField destinationTextField;
    TextField fileLocationTextField;
    ProgressBar directoryProgressBar;
    ProgressBar progressBar;
    RadioButton moveRadioButton;
    RadioButton copyRadioButton;
    Button masterKeyBrowseButton;
    Button submitButton;
    Button fileLocationBrowseButton;
    Button reportBrowseButton;
    Button createReportButton;
    ListView<String> dirListView;
    ComboBox<MoveMode> modeComboBox;

    public FileMoverModeSetup(MoveMode currentMOveMode) {
        this.currentMoveMode = currentMOveMode;
    }

    public void setObjects(ProgressBar directoryProgressBar, Pane directoryPane,
            Label destinationIntroLabel, TextField reportTextField,
            ComboBox<MoveMode> modeComboBox, Label masterKeyLabel,
            Label familiesPrefixLabel, TextField destinationTextField,
            Label directoryLabel, Button fileLocationBrowseButton,
            TextField fileLocationTextField, RadioButton moveRadioButton,
            RadioButton copyRadioButton, Button masterKeyBrowseButton,
            TextField masterKeyTextField, TextField familiesPrefixTextField,
            Label percentProgressLabel, Label currentProgressFileLabel,
            ProgressBar progressBar, Pane ModePane, Button submitButton,
            Pane userInputPane, Label fileLocationLabel,
            Button reportBrowseButton, ListView<String> dirListView,
            Button createReportButton) {

        this.directoryPane = directoryPane;
        this.ModePane = ModePane;
        this.userInputPane = userInputPane;
        this.destinationIntroLabel = destinationIntroLabel;
        this.masterKeyLabel = masterKeyLabel;
        this.familiesPrefixLabel = familiesPrefixLabel;
        this.directoryLabel = directoryLabel;
        this.percentProgressLabel = percentProgressLabel;
        this.currentProgressFileLabel = currentProgressFileLabel;
        this.fileLocationLabel = fileLocationLabel;
        this.masterKeyTextField = masterKeyTextField;
        this.familiesPrefixTextField = familiesPrefixTextField;
        this.reportTextField = reportTextField;
        this.destinationTextField = destinationTextField;
        this.fileLocationTextField = fileLocationTextField;
        this.directoryProgressBar = directoryProgressBar;
        this.progressBar = progressBar;
        this.moveRadioButton = moveRadioButton;
        this.copyRadioButton = copyRadioButton;
        this.masterKeyBrowseButton = masterKeyBrowseButton;
        this.submitButton = submitButton;
        this.fileLocationBrowseButton = fileLocationBrowseButton;
        this.reportBrowseButton = reportBrowseButton;
        this.dirListView = dirListView;
        this.modeComboBox = modeComboBox;
        this.createReportButton = createReportButton;
    }

    public void activate() {
        reportTextField.getStylesheets().clear();
        familiesPrefixTextField.clear();
        DirectoryChooser chooser = new DirectoryChooser();
        if (currentMoveMode.getInitialDirectory() != null) {
            chooser.setInitialDirectory(new File(currentMoveMode.getInitialDirectory()));
        }

        if (!currentMoveMode.equals(MoveMode.ALL_FILES)) {
            setBottomFieldsDisabled(true);
        }

        switch (currentMoveMode) {
            case ALL_FILES:
                setReportFieldsVisible(false);
                setFamilyFieldsVisible(false);
                setBottomFieldsDisabled(false);
                createReportButton.setVisible(false);
                directoryProgressBar.setOpacity(1);
                directoryPane.getChildren().clear();
                destinationIntroLabel.setText("Destination:");
                break;

            case QC_REPORT:
                setReportFieldsVisible(true);
                setFamilyFieldsVisible(false);
                createReportButton.setVisible(false);
                directoryProgressBar.setOpacity(1);
                directoryPane.getChildren().clear();
                reportTextField.setPromptText("Select a qc report...");
                destinationIntroLabel.setText("Destination:");
                break;

            case QC_REPORT_BY_FAMILY:
                setReportFieldsVisible(true);
                setFamilyFieldsVisible(true);
                createReportButton.setVisible(false);
                directoryProgressBar.setOpacity(0);
                masterKeyLabel.setText("Master Key:");
//                familiesPrefixLabel.setText("Families Prefix:");
                reportTextField.setPromptText("Select an qc report...");
                destinationIntroLabel.setText("Destination of \"dataset_to_PI\" Folder :");
                break;

            case SEQ_RUN_CLEAN_UP:
            case FAST_Q_CLEAN_UP:
//                break;
            case NOVA_CLEAN_UP:
            case MISEQ_RUN_CLEAN_UP:

                customCleanUpDir = chooser.showDialog(null);
                if (customCleanUpDir == null) {
                    modeComboBox.getSelectionModel().clearSelection();
                } else {

                    setBottomFieldsDisabled(false);
                    setReportFieldsVisible(true);
                    setCleanUpFieldsVisible(true);
                    directoryProgressBar.setOpacity(0);
                    reportTextField.setPromptText("Select an exclusion file...");
                    destinationIntroLabel.setText("Destination of files after clean up:");
                }
                break;

            case BAD_FLOW_CELL_CLEAN_UP:
            case NOVA_BAD_FLOW_CELL_CLEAN_UP:

                customCleanUpDir = chooser.showDialog(null);
                if (customCleanUpDir == null) {
                    modeComboBox.getSelectionModel().clearSelection();
                } else {

                    setBottomFieldsDisabled(false);
                    setReportFieldsVisible(true);
                    setCleanUpFieldsVisible(true);
                    directoryProgressBar.setOpacity(0);
                    reportTextField.setPromptText("Select an exclusion file...");
                    destinationIntroLabel.setText("Destination of bad flowcell cleanup :");
                }
                break;

            case SEQ_BCL_CLEAN_UP:
//            case NOVA_BCL_CLEAN_UP:
            case MISEQ_BCL_CLEAN_UP:
            case CUSTOM_BCL_CLEAN_UP:
                customCleanUpDir = chooser.showDialog(null);
                if (customCleanUpDir == null) {
                    modeComboBox.getSelectionModel().clearSelection();
                } else {
                    setBottomFieldsDisabled(false);
                    setReportFieldsVisible(true);
                    setCleanUpFieldsVisible(true);
                    directoryProgressBar.setOpacity(0);
                    reportTextField.setPromptText("Select an exclusion file...");
                    destinationIntroLabel.setText("Destination of bcl files after clean up :");
                }
                break;
        }
    }

    public void resetUserInputPane() {
        fileLocationLabel.setText("");
        fileLocationTextField.clear();
        directoryPane.getChildren().clear();
        destinationTextField.clear();
        masterKeyTextField.clear();
        familiesPrefixTextField.clear();
    }

    public void setAllFieldsDisabled(boolean enable) {
        userInputPane.setDisable(enable);
        submitButton.setDisable(enable);
        ModePane.setDisable(enable);
        progressBar.setVisible(enable);
        currentProgressFileLabel.setVisible(enable);
        percentProgressLabel.setVisible(enable);
    }

    public void setReportFieldsVisible(boolean visible) {
        reportBrowseButton.setVisible(visible);
        reportTextField.setVisible(visible);

        if (visible) {
            modeComboBox.setPrefWidth(456);
            moveRadioButton.setLayoutX(630);
            moveRadioButton.setLayoutY(15);
            copyRadioButton.setLayoutX(696);
            copyRadioButton.setLayoutY(15);
        } else {
            modeComboBox.setPrefWidth(896);
            moveRadioButton.setLayoutX(404);
            moveRadioButton.setLayoutY(51);
            copyRadioButton.setLayoutX(470);
            copyRadioButton.setLayoutY(51);
        }
    }

    public void setFamilyFieldsVisible(boolean visible) {
        if (visible) {
            modeComboBox.setPrefWidth(896);
            dirListView.setVisible(false);
        }
        directoryLabel.setVisible(!visible);
        fileLocationBrowseButton.setVisible(!visible);
        fileLocationTextField.setVisible(!visible);
        moveRadioButton.setVisible(!visible);
        copyRadioButton.setVisible(!visible);
        masterKeyLabel.setVisible(visible);
        masterKeyBrowseButton.setVisible(visible);
        masterKeyTextField.setVisible(visible);
        familiesPrefixLabel.setVisible(false);
        familiesPrefixTextField.setVisible(false);
        directoryProgressBar.setVisible(visible);
    }

    public void setCleanUpFieldsVisible(boolean visible) {
        if (visible) {
            modeComboBox.setPrefWidth(896);
            familiesPrefixLabel.setText("Search:");
            masterKeyLabel.setText("Run Folder:");


            if(currentMoveMode.equals(MoveMode.FAST_Q_CLEAN_UP)){

            masterKeyLabel.setText("Inclusion .csv");
            }

            //TODO: Add BCL Destination path fix
            if (MoveMode.MOVE_AFTER_CLEAN.contains(currentMoveMode)) {
                destinationTextField.setText(customCleanUpDir.getAbsolutePath());
                destinationTextField.setDisable(false);

            } else if (MoveMode.STATIONARY_MODES.contains(currentMoveMode)) {
                if (customCleanUpDir == null) {
                    destinationTextField.setText(currentMoveMode.getFinalDirectory());
                    masterKeyLabel.setText("Clean Up Folder:");
                } else {
                    destinationTextField.setText(customCleanUpDir.getAbsolutePath());
                }
                destinationTextField.setDisable(true);
            } else {
                destinationTextField.setText(currentMoveMode.getFinalDirectory());
                destinationTextField.setDisable(false);
            }
        }
        createReportButton.setVisible(visible);
        directoryLabel.setVisible(!visible);
        fileLocationBrowseButton.setVisible(!visible);
        fileLocationTextField.setVisible(!visible);
        moveRadioButton.setVisible(!visible);
        copyRadioButton.setVisible(!visible);
        masterKeyLabel.setVisible(visible);
        masterKeyBrowseButton.setVisible(visible);
        masterKeyTextField.setVisible(visible);
        familiesPrefixLabel.setVisible(visible);
        familiesPrefixTextField.setVisible(visible);
        directoryProgressBar.setVisible(visible);
    }

    public void setBottomFieldsDisabled(boolean enable) {
        userInputPane.setDisable(enable);
        submitButton.setDisable(enable);
    }

    public File getCustomCleanUpDir() {
        return customCleanUpDir;
    }

}
