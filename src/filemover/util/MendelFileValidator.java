/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;

/**
 *
 * @author kduerr1
 */
public class MendelFileValidator {

    private final FileMoverFile fileMoverFile;

    public MendelFileValidator(FileMoverFile fileMoverFile) {
        this.fileMoverFile = fileMoverFile;
    }

    public boolean isValid() {
        boolean isValidMendelFile = false;
        if (fileMoverFile.getDirectory().toString().endsWith("BAM\\AGGREGATE")) {
            isValidMendelFile = true;
        }
         if (fileMoverFile.getDirectory().toString().endsWith("CRAM")) {
            isValidMendelFile = true;
        }
        if (fileMoverFile.getDirectory().toString().endsWith("VCF\\RELEASE\\FILTERED_ON_BAIT")) {
            isValidMendelFile = true;
        }
//        if (fileMoverFile.getDirectory().toString().endsWith("VCF\\RELEASE\\FILTERED_ON_BAIT\\TABIX")) {
//            isValidMendelFile = true;
//        }
        if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\ANNOVAR")) {
            isValidMendelFile = true;
        }
        if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\DEPTH_OF_COVERAGE\\UCSC")) {
//        if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\GENES_COVERAGE\\UCSC")) {
//        if (fileMoverFile.getDirectory().toString().endsWith("REPORTS\\GENES_COVERAGE\\UCSC_EXONS")) {
            isValidMendelFile = true;
        }
        return isValidMendelFile;
    }
}
