/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileType;
import filemover.FileTypeBox;
import filemover.MoveMode;
import java.util.Set;
import javafx.scene.layout.GridPane;

/**
 *
 * @author kduerr1
 */
public class GridPaneFiller {

    private final GridPane fileTypeGridPane;
    private final MoveMode currentMoveMode;
    private final Set<FileType> fileTypes;

    public GridPaneFiller(GridPane fileTypeGridPane, MoveMode currentMoveMode, Set<FileType> fileTypes) {
        this.fileTypeGridPane = fileTypeGridPane;
        this.currentMoveMode = currentMoveMode;
        this.fileTypes = fileTypes;
    }

    public void fillPane() {
        int count = 0;
        int row = 1;
        int column = 1;
        fileTypeGridPane.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(167,207,223,1) 0%,rgba(35,83,138,1) 100%);");

        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY) || currentMoveMode.equals(MoveMode.QC_REPORT)) {
              for (FileType fileType : fileTypes) {
                count++;
                if (count == (row * 4) + 1) {
                    column = 1;
                }
                if (count == (row * 4) + 1) {
                    row++;
                }
                FileTypeBox fileTypeBox = new FileTypeBox(fileType, currentMoveMode);
                fileTypeGridPane.add(fileTypeBox.createFileTypeBox(), column - 1, row - 1);
                column++;
            }
           
        } else {
           for (FileType fileType : fileTypes) {
                count++;
                if (count == (row * 3) + 1) {
                    column = 1;
                }
                if (count == (row * 3) + 1) {
                    row++;
                }
                FileTypeBox fileTypeBox = new FileTypeBox(fileType, currentMoveMode);
                fileTypeGridPane.add(fileTypeBox.createFileTypeBox(), column - 1, row - 1);
                column++;
            }
        }
    }
}
