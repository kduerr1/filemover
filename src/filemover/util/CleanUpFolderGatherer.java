/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverController;
import filemover.MoveMode;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * @author kduerr1
 */
public class CleanUpFolderGatherer {

    //    private final int BCL_CLEAN_UP_WAITING_MONTHS = 1; //for testing
    private final int BCL_CLEAN_UP_WAITING_MONTHS = 6;
    //    private final int CLEAN_UP_WAITING_DAYS = 20; //for testing
    private final int CLEAN_UP_WAITING_DAYS = 56;
    final List<File> cleanUpDirs = new ArrayList<>();

    private final MoveMode currentMoveMode;
    private List<String> cleanUpStrings;
    private File customCleanUpDir = null;

    public CleanUpFolderGatherer(MoveMode currentMoveMode, List<String> cleanUpStrings) {
        this.currentMoveMode = currentMoveMode;
        this.cleanUpStrings = cleanUpStrings;
    }

    public List<File> getCleanUpDirectoryList() {
        final List<File> allDirs = new ArrayList<>();
//        final List<File> cleanUpDirs = new ArrayList<>();
        cleanUpStrings = new ArrayList<>();
        if (customCleanUpDir != null) {
//        if (MoveMode.CUSTOM_BCL_CLEAN_UP.equals(currentMoveMode) && customCleanUpDir != null) {
            allDirs.addAll(Arrays.asList(customCleanUpDir.listFiles()));
        } else {
            try {
                allDirs.addAll(Arrays.asList(new File(currentMoveMode.getInitialDirectory()).listFiles()));
            } catch (Exception e) {
                System.out.println("ERROR: Unable to reach directory");
//                e.printStackTrace();
            }
        }

        for (File folder : allDirs) {
            if (folder.getName().length() > 7 || MoveMode.FAST_Q_CLEAN_UP.equals(currentMoveMode)) {
                if (!isRunCompleted(folder) || MoveMode.FAST_Q_CLEAN_UP.equals(currentMoveMode)) {

                    if (MoveMode.getBCL_MODES().contains(currentMoveMode) || MoveMode.BAD_FLOW_CELL_CLEAN_UP.equals(currentMoveMode)) {
                        if (new File(folder, "Data").exists()) {
                            String dateString = folder.getName().substring(0, 6);
                            if (isValidDate(dateString)) {
                                cleanUpDirs.add(folder);
                                cleanUpStrings.add(folder.getName());
                            }
                        }
                    } else {
                        if (MoveMode.FAST_Q_CLEAN_UP.equals(currentMoveMode)) {
                            if (folder.isDirectory()) {
                                cleanUpDirs.add(folder);
                                cleanUpStrings.add(folder.getName());
                            }
                        } else {
                            String dateString = folder.getName().substring(0, 6);
                            if (isValidDate(dateString)) {
                                cleanUpDirs.add(folder);
                                cleanUpStrings.add(folder.getName());
                            }
                        }
                    }
                }
            }
        }
        Collections.sort(cleanUpDirs);
        return cleanUpDirs;
    }

    private boolean isRunCompleted(File dir) {
        boolean isComplete = false;
        if (dir.listFiles() != null) {

            for (File file : dir.listFiles()) {
                String fileName;

                String bclString = "Deleted_During_BCL_Clean_Up";
                if (MoveMode.getBCL_MODES().contains(currentMoveMode)) {
                    fileName = bclString;
                }
                else {
                    fileName = FileMoverController.COMPLETED_FILE_NAME;
                }

                if (file.getName().contains(fileName)) {
                    isComplete = true;
                }
            }
        }
        return isComplete;
    }

    private boolean isValidDate(String dateString) {
        if (dateString.matches("^[0-9]{6}")) {
            try {
                Date creationDate = new SimpleDateFormat("yyMMdd", Locale.ENGLISH).parse(dateString);
                Calendar calendar = new GregorianCalendar();
                if (MoveMode.getBCL_MODES().contains(currentMoveMode)) {
                    calendar.add(Calendar.MONTH, -BCL_CLEAN_UP_WAITING_MONTHS);
                } else if (MoveMode.BAD_FLOW_CELL_MODES.contains(currentMoveMode)) {
                    return true;
                } else {
                    calendar.add(Calendar.DATE, -CLEAN_UP_WAITING_DAYS);
                }
                Date d = calendar.getTime();
                if (creationDate.before(d)) {
                    return true;
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        return false;
    }

    public List<String> getCleanUpStrings() {
        return cleanUpStrings;
    }

    public void setCustomBclDir(File customBclDir) {
        this.customCleanUpDir = customBclDir;
    }

}
