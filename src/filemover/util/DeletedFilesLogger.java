/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;
import filemover.MoveMode;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author kduerr1
 */
public class DeletedFilesLogger {

    private final Map<String, Set<FileMoverFile>> directoryToFileMoverFiles;
    private final MoveMode currentMoveMode;
    private final HashSet<String> cleanUpSelectedDirs;
    private File customCleanUpDir = null;

    public DeletedFilesLogger(Map<String, Set<FileMoverFile>> directoryToFileMoverFiles, MoveMode currentMoveMode, HashSet<String> cleanUpSelectedDirs) {
        this.directoryToFileMoverFiles = directoryToFileMoverFiles;
        this.currentMoveMode = currentMoveMode;
        this.cleanUpSelectedDirs = cleanUpSelectedDirs;
    }

    public void writeLogs() throws IOException {
        for (Map.Entry<String, Set<FileMoverFile>> entry : directoryToFileMoverFiles.entrySet()) {
            String directory = entry.getKey();
            if (cleanUpSelectedDirs.contains(directory)) {

                Set<FileMoverFile> set = entry.getValue();

                StringBuilder sb = new StringBuilder();

                for (FileMoverFile fileMoverFile : set) {
                    sb.append(fileMoverFile.getFile().getAbsolutePath());
                    sb.append("\n");
                }

                Date date = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyMMdd_kkmm");

                File outputFile;
                if (MoveMode.getBCL_MODES().contains(currentMoveMode)) {
//                    outputFile = new File(currentMoveMode.getFinalDirectory() + "\\" + directory + "\\Deleted_During_BCL_Clean_Up_" + dateFormat.format(date) + ".csv");
                    if (customCleanUpDir != null) {
                        outputFile = new File(customCleanUpDir.getAbsolutePath() + "\\" + directory + "\\Deleted_During_BCL_Clean_Up_" + dateFormat.format(date) + ".csv");

                    } else {

                        outputFile = new File(currentMoveMode.getInitialDirectory() + "\\" + directory + "\\Deleted_During_BCL_Clean_Up_" + dateFormat.format(date) + ".csv");
                    }

                } else {
//                    outputFile = new File(currentMoveMode.getFinalDirectory() + "\\" + directory + "\\Deleted_During_Clean_Up_" + dateFormat.format(date) + ".csv");
                    outputFile = new File(currentMoveMode.getInitialDirectory() + "\\" + directory + "\\Deleted_During_Clean_Up_" + dateFormat.format(date) + ".csv");
                }

                FileWriter fileWriter = null;
                fileWriter = new FileWriter(outputFile);
                fileWriter.write(sb.toString());
                fileWriter.close();
            }
        }
    }

    public void setCustomBclDir(File customBclDir) {
        this.customCleanUpDir = customBclDir;
    }

}
