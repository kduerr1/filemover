/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;
import filemover.FileType;
import filemover.MoveMode;

import java.io.File;
import java.util.*;

/**
 * @author kduerr1
 */
public class CleanUpFileMoverFileAdder {

    private final File directory;
    private final MoveMode moveMode;
    private final List<File> scannedDirs;
    private List<File> fileList;
    private Map<String, List<String>> fastqMap;

    public CleanUpFileMoverFileAdder(File directory, MoveMode moveMode, Map<String, List<String>> fastqMap) {
        this.directory = directory;
        this.moveMode = moveMode;
        this.fastqMap = fastqMap;
        scannedDirs = new ArrayList<>();
    }

    public Set<FileMoverFile> getFileMoverFiles() {
        Set<FileMoverFile> fileMoverFiles = new HashSet<>();
        String dirString = directory.getAbsolutePath();

        for (FileType fileType : moveMode.getFileTypeSet()) {
            if (fileType.isDir()) {
                File fileTypeFile = new File(dirString + "\\" + fileType.getExtension());
                if (fileTypeFile.exists()) {
                    List<File> dirFiles = new ArrayList<>();
                    fileList = new ArrayList<>();
                    dirFiles = getAllFiles(fileTypeFile.getAbsolutePath(), fileType);

                    for (File file : dirFiles) {
                        FileMoverFile fileMoverFile = new FileMoverFile(file, fileType);
                        fileMoverFiles.add(fileMoverFile);
                    }
                }
            } else {
                fileList = new ArrayList<>();

                List<File> files = getAllFiles(dirString, fileType);
                for (File file : files) {
                    FileMoverFile fileMoverFile = new FileMoverFile(file, fileType);
                    fileMoverFiles.add(fileMoverFile);
                }
                //not Dir// cannot be file in clean up
                //CHANGES FOR FASTQ
            }
        }
        return fileMoverFiles;
    }

    public List<File> getAllFiles(String path, FileType fileType) {

        File root = new File(path);

        for (File f : root.listFiles()) {
            if (f.isDirectory() && !scannedDirs.contains(f)) {
                scannedDirs.add(f);
                switch (moveMode) {
                    case SEQ_RUN_CLEAN_UP:
                        if (fileType.equals(FileType.DATA_INTENSITIES)) {
                            getAllFiles(f.getAbsolutePath(), fileType);
                            if (f.getName().startsWith("C") && f.getParentFile().getAbsolutePath().contains("Data\\Intensities\\L00")) {
                                getAllFiles(f.getAbsolutePath(), fileType);
                                addFile(f);
                            } else {
                                continue;
                            }
                        } else {
                            getAllFiles(f.getAbsolutePath(), fileType);
                        }
                        continue;
                    case MISEQ_RUN_CLEAN_UP:
                        if (fileType.equals(FileType.DATA_INTENSITIES_BASECALLS)) {
                            getAllFiles(f.getAbsolutePath(), fileType);
                            if (f.getAbsolutePath().contains("Alignment")) {
                                addFile(f);
                            }
                        } else {
                            getAllFiles(f.getAbsolutePath(), fileType);
                        }
                        continue;
                    case NOVA_CLEAN_UP:
                    case FAST_Q_CLEAN_UP:
                    case SEQ_BCL_CLEAN_UP:
//                    case NOVA_BCL_CLEAN_UP:
                    case MISEQ_BCL_CLEAN_UP:
                    case CUSTOM_BCL_CLEAN_UP:
                        getAllFiles(f.getAbsolutePath(), fileType);
                        continue;
                    case BAD_FLOW_CELL_CLEAN_UP:
                    case NOVA_BAD_FLOW_CELL_CLEAN_UP:
                        getAllFiles(f.getAbsolutePath(), fileType);
                }
                continue;

            } else {
                switch (moveMode) {
                    case NOVA_CLEAN_UP:
                        if (fileType.equals(FileType.LOGS)) {
                            if (f.getName().endsWith("log") && f.getParent().endsWith("\\Logs")) {
                                fileList.add(f);
                            }
                        } else {
                            fileList.add(f);
                        }
                        continue;
                    case SEQ_RUN_CLEAN_UP:
                        if (fileType.equals(FileType.DATA_INTENSITIES)) {
//                            continue;
                        } else if (fileType.equals(FileType.LOGS)) {
                            if (f.getName().endsWith("log")) {
                                fileList.add(f);
                            }
                        } else {
                            fileList.add(f);
                        }
                        continue;
                    case MISEQ_RUN_CLEAN_UP:
                        if (fileType.equals(FileType.DATA_INTENSITIES_BASECALLS)) {
                            if (f.getName().contains(".fastq")) {
                                fileList.add(f);
                            }
                        } else {
                            fileList.add(f);
                        }
                        continue;
                    case MISEQ_BCL_CLEAN_UP:
                    case SEQ_BCL_CLEAN_UP:
                    case CUSTOM_BCL_CLEAN_UP:
                        fileList.add(f);
                        continue;
                    case FAST_Q_CLEAN_UP:
                        if(fastqMap != null){
                        fastqMap.forEach((s, strings) -> {
                            if (f.getAbsoluteFile().toString().contains(s)) {
                                strings.forEach(s1 -> {
                                    if (f.getName().contains(s1)) {
                                        fileList.add(f);
                                    }
                                });
                            }
                        });
                        }

                        continue;
//                    case NOVA_BCL_CLEAN_UP:
                    case BAD_FLOW_CELL_CLEAN_UP:
                    case NOVA_BAD_FLOW_CELL_CLEAN_UP:
                        if (fileType.equals(FileType.LOGS)) {
                            if (f.getName().endsWith("log")) {
                                fileList.add(f);
                            }
                        } else {
                            fileList.add(f);
                        }
                }
            }
        }
        return fileList;
    }

    private void addFile(File f) {
        for (File file : f.listFiles()) {
            if (file.isFile()) {
                fileList.add(file);
            }
        }
    }
}
