package filemover.util;

import filemover.Job;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TreeItem;

public class TreeViewFiller {

    private List<CheckBoxTreeItem<File>> treeItems = new ArrayList<>();
    ProgressBar directoryProgressBar;
    ProgressBar fileTypeProgressBar;

    public TreeViewFiller(ProgressBar directoryProgressBar, ProgressBar fileTypeProgressBar) {
        this.directoryProgressBar = directoryProgressBar;
        this.fileTypeProgressBar = fileTypeProgressBar;
    }

    public CheckBoxTreeItem<File> createNode(final File f, final Job job) {
        return new CheckBoxTreeItem<File>(f) {
            private boolean isLeaf;
            private boolean isFirstTimeChildren = true;
            private boolean isFirstTimeLeaf = true;

            @Override
            public ObservableList<TreeItem<File>> getChildren() {
                if (isFirstTimeChildren) {
                    isFirstTimeChildren = false;
                    super.getChildren().setAll(buildChildren(this));
                }
                return super.getChildren();
            }

            @Override
            public boolean isLeaf() {
                if (isFirstTimeLeaf) {
                    isFirstTimeLeaf = false;
                    File f = (File) getValue();
                    isLeaf = f.isFile();
                }
                return isLeaf;
            }

            private ObservableList<TreeItem<File>> buildChildren(TreeItem<File> TreeItem) {
                directoryProgressBar.setVisible(true);
                fileTypeProgressBar.setVisible(true);
                File f = TreeItem.getValue();
                if (f != null && f.isDirectory()) {
                    File[] files = f.listFiles();
                    if (files != null) {
                        ObservableList<TreeItem<File>> children = FXCollections.observableArrayList();
                        for (File childFile : files) {
                            if (job.isCancelJob()) {
                                return children;
                            }
                            if (childFile.isDirectory()) {
                                CheckBoxTreeItem<File> checkBoxTreeItem = createNode(childFile, job);
                                checkBoxTreeItem.setSelected(true);
                                children.add(checkBoxTreeItem);
                                treeItems.add(checkBoxTreeItem);
                            }
                        }
                        return children;
                    }
                }
                return FXCollections.emptyObservableList();
            }
        };
    }

    public List<CheckBoxTreeItem<File>> getTreeItems() {
        return treeItems;
    }
}
