/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;
import filemover.FileMoverFile;
import filemover.FileType;
import filemover.FileType;
import filemover.FileTypeBox;
import filemover.FileTypeBox;
import filemover.Job;
import filemover.Job;
import filemover.MoveMode;
import filemover.MoveMode;
import filemover.ProjectBox;
import filemover.ProjectBox;
import java.io.File;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author kduerr1
 */
public class DirectoryHandler {

    private Set<FileMoverFile> allFileMoverFilesList;
    private TreeView directoryTreeView;
    private final Pane directoryPane;
    private final GridPane fileTypeGridPane;
    private final MoveMode currentMoveMode;
    private final Deque<Job> jobQue;
    private List<File> checkedDirectoryList;
    private Set<FileMoverFile> checkedDirectoryFileList = new HashSet<>();
    private final ProgressBar fileTypeProgressBar;
    private final ProgressBar directoryProgressBar;

    public DirectoryHandler(Set<FileMoverFile> allFilesList, TreeView directoryTreeView, Pane directoryPane, GridPane fileTypeGridPane, MoveMode currentMoveMode, Deque<Job> jobQue, ProgressBar fileTypeProgressBar, ProgressBar directoryProgressBar) {
        this.allFileMoverFilesList = allFilesList;
        this.directoryTreeView = directoryTreeView;
        this.directoryPane = directoryPane;
        this.fileTypeGridPane = fileTypeGridPane;
        this.currentMoveMode = currentMoveMode;
        this.jobQue = jobQue;
        this.fileTypeProgressBar = fileTypeProgressBar;
        this.directoryProgressBar = directoryProgressBar;
    }

    public void createTree(final File directoryLocation, Job job) {
        final TreeViewFiller treeViewFiller = new TreeViewFiller(fileTypeProgressBar, directoryProgressBar);
        CheckBoxTreeItem<File> root = treeViewFiller.createNode(directoryLocation, job);
        root.setSelected(true);

        directoryTreeView = new TreeView<>(root);
        directoryTreeView.setRoot(root);
        directoryTreeView.setShowRoot(false);
        directoryTreeView.setCellFactory(CheckBoxTreeCell.forTreeView());
        directoryTreeView.setMinWidth(directoryPane.getWidth() - 1);
        directoryTreeView.setMaxHeight(directoryPane.getHeight());

        directoryTreeView.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                jobQue.add(new Job(directoryLocation.toString(), new Job.JobRunnable() {

                    @Override
                    public void run(Job job) {
                        fileTypeProgressBar.setVisible(true);

                        List<File> unCheckedDirectories = new ArrayList<>();
                        for (CheckBoxTreeItem<File> checkBoxTreeItem : treeViewFiller.getTreeItems()) {
                            if (!checkBoxTreeItem.isSelected()) {
                                unCheckedDirectories.add(checkBoxTreeItem.getValue());
                            }
                        }

                        checkedDirectoryList = new ArrayList<>();
                        checkedDirectoryFileList = new HashSet<>();
                        final Map<FileType, Integer> fileTypeCountMap;
                        fileTypeCountMap = new HashMap<>();
                        for (FileType fileType : currentMoveMode.getFileTypeSet()) {
                            fileTypeCountMap.put(fileType, 0);
                        }
                        List<File> removedFiles = new ArrayList<>();
                        for (FileMoverFile fileMoverFile : allFileMoverFilesList) {
                            if (unCheckedDirectories.contains(fileMoverFile.getDirectory())) {
                                removedFiles.add(fileMoverFile.getFile());
                            }
                            if (fileMoverFile.getFile().isFile() && !removedFiles.contains(fileMoverFile.getFile())) {
                                checkedDirectoryFileList.add(fileMoverFile);
                                fileTypeCountMap.put(fileMoverFile.getFileType(), fileTypeCountMap.get(fileMoverFile.getFileType()) + 1);
                                checkedDirectoryList.add(fileMoverFile.getDirectory());
                            }

                        }

                        for (Node fileTypeNode : fileTypeGridPane.getChildren()) {
                            final FileTypeBox fileTypeBox = (FileTypeBox) fileTypeNode;
                            if (fileTypeCountMap.keySet().contains(fileTypeBox.getFileType())) {
                                Platform.runLater(new Runnable() {

                                    @Override
                                    public void run() {
                                        setFileTypeBoxCount(fileTypeBox, fileTypeCountMap.get(fileTypeBox.getFileType()));
                                    }
                                });
                            }
                        }
                        fileTypeProgressBar.setVisible(false);
                    }
                }));
            }
        });

        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                directoryPane.getChildren().add(directoryTreeView);
            }
        });
    }

    public void setFileTypeBoxCount(FileTypeBox fileTypeBox, int count) {
        VBox vBox = (VBox) fileTypeBox.getChildren().get(0);
        HBox hBox = (HBox) vBox.getChildren().get(2);
        Label label;
        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            label = (Label) hBox.getChildren().get(3);

        } else if(MoveMode.QC_REPORT_BY_FAMILY == currentMoveMode ||  currentMoveMode.equals(MoveMode.QC_REPORT)) {
            label = (Label) hBox.getChildren().get(1);
        } else {
            label = (Label) hBox.getChildren().get(2);
        }
        label.setText(String.valueOf(count));
        if (count > 0) {
            label.setStyle("-fx-text-fill: GREEN");
        } else {
            label.setStyle("-fx-text-fill: RED");
        }
    }

    public void populateDirectoryPaneWithProjects(Map<String, List<String>> projectToSampleMap, Map<String, List<String>> projectToFamilyMap) {
        //TODO COMPARE QC REPORT SAMPLE TOTAL?

        int sampleCount = 0;
        int familyCount = 0;

        for (Map.Entry<String, List<String>> entry : projectToSampleMap.entrySet()) {
            List<String> list = entry.getValue();
            sampleCount += list.size();
        }

        for (Map.Entry<String, List<String>> entry : projectToFamilyMap.entrySet()) {
            List<String> list = entry.getValue();
            familyCount += list.size();

        }

        directoryPane.getChildren().clear();
        ProjectBox projectBox = new ProjectBox(projectToFamilyMap, projectToSampleMap);
        VBox directoryVBox = new VBox();

        for (String project : projectToSampleMap.keySet()) {
            directoryVBox.getChildren().add(projectBox.createProjectBox(project));
        }
        ScrollPane scrollPane = new ScrollPane();
        VBox directoryHolderVBox = new VBox();
        GridPane directoryStatCountGridPane = new GridPane();
        scrollPane.setContent(directoryVBox);
//        scrollPane.setMinWidth(609);
        scrollPane.setMinWidth(897);
        scrollPane.setMinHeight(100);
        directoryStatCountGridPane.setMinHeight(45);
        directoryStatCountGridPane.setGridLinesVisible(true);
        directoryHolderVBox.getChildren().add(scrollPane);

        VBox sampleCountVBox = new VBox();
        VBox projectCountVBox = new VBox();
        VBox familyCountVBox = new VBox();

        HBox sampleCountHBox = new HBox();
        HBox projectCountHBox = new HBox();
        HBox familyCountHBox = new HBox();

        Pane sampleCountAboveBufferPane = new Pane();
        Pane projectCountAboveBufferPane = new Pane();
        Pane familyCounttAboveBufferPane = new Pane();

        Pane sampleCountLeftBufferPane = new Pane();
        Pane projectCountLeftBufferPane = new Pane();
        Pane familyCounttLeftBufferPane = new Pane();

        Label projectCountLabel;
        Label familyCountLabel;
        Label sampleCountLabel;

        if (projectToSampleMap.keySet().size() == 1) {
            projectCountLabel = new Label(String.valueOf(projectToSampleMap.keySet().size()) + " Total Project");
        } else {
            projectCountLabel = new Label(String.valueOf(projectToSampleMap.keySet().size()) + " Total Projects");
        }

        if (familyCount == 1) {
            familyCountLabel = new Label(familyCount + " Total Family");
        } else {
            familyCountLabel = new Label(familyCount + " Total Families");
        }

        if (sampleCount == 1) {
            sampleCountLabel = new Label(sampleCount + " Total Sample");
        } else {
            sampleCountLabel = new Label(sampleCount + " Total Samples");
        }

        sampleCountLabel.setFont(Font.font("System", FontWeight.BOLD, 16));
        projectCountLabel.setFont(Font.font("System", FontWeight.BOLD, 16));
        familyCountLabel.setFont(Font.font("System", FontWeight.BOLD, 16));

        sampleCountAboveBufferPane.setMinHeight(13);
        projectCountAboveBufferPane.setMinHeight(13);
        familyCounttAboveBufferPane.setMinHeight(13);

        sampleCountLeftBufferPane.setMinWidth(80);
        projectCountLeftBufferPane.setMinWidth(85);
        familyCounttLeftBufferPane.setMinWidth(70);

        sampleCountVBox.getChildren().add(sampleCountAboveBufferPane);
        projectCountVBox.getChildren().add(projectCountAboveBufferPane);
        familyCountVBox.getChildren().add(familyCounttAboveBufferPane);

        sampleCountHBox.getChildren().add(sampleCountLeftBufferPane);
        projectCountHBox.getChildren().add(projectCountLeftBufferPane);
        familyCountHBox.getChildren().add(familyCounttLeftBufferPane);

        sampleCountHBox.getChildren().add(sampleCountLabel);
        projectCountHBox.getChildren().add(projectCountLabel);
        familyCountHBox.getChildren().add(familyCountLabel);

        sampleCountVBox.getChildren().add(sampleCountHBox);
        projectCountVBox.getChildren().add(projectCountHBox);
        familyCountVBox.getChildren().add(familyCountHBox);

        sampleCountVBox.setMinHeight(48);
        projectCountVBox.setMinHeight(48);
        familyCountVBox.setMinHeight(48);
//        sampleCountVBox.setMinWidth(203);
//        familyCountVBox.setMinWidth(203);
//        projectCountVBox.setMinWidth(203);

        int counterWidth = 299;
        sampleCountVBox.setMinWidth(counterWidth);
        familyCountVBox.setMinWidth(counterWidth);
        projectCountVBox.setMinWidth(counterWidth);

        directoryStatCountGridPane.add(projectCountVBox, 1, 1);
        directoryStatCountGridPane.add(familyCountVBox, 2, 1);
        directoryStatCountGridPane.add(sampleCountVBox, 3, 1);
        directoryStatCountGridPane.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(211,199,216,1) 0%,rgba(207,148,221,1) 100%);");

        directoryHolderVBox.getChildren().add(directoryStatCountGridPane);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scrollPane.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(167,207,223,1) 0%,rgba(35,83,138,1) 100%);");

        directoryPane.getChildren().add(directoryHolderVBox);
    }

    public Set<FileMoverFile> getCheckedDirectoryFileList() {
        return checkedDirectoryFileList;
    }

    public void setMovingFileList(Set<FileMoverFile> movingFileList) {
        this.checkedDirectoryFileList = movingFileList;
    }

    public List<File> getCheckedDirectoryList() {
        return checkedDirectoryList;
    }

    public void setCheckedDirectoryList(List<File> checkedDirectoryList) {
        this.checkedDirectoryList = checkedDirectoryList;
    }
}
