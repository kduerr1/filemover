/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import filemover.FileMoverFile;
import filemover.FileMoverFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author bmyers21
 */
public class ExclusionFileParser {

    private Set<FileMoverFile> filesToDelete = new HashSet<>();
    private Set<FileMoverFile> excludedFiles = new HashSet<>();

    private final List<String> excludedFolderNames;
    private final Set<FileMoverFile> allFileMoverFilesList;
    private final File exclusionFile;

    public ExclusionFileParser(Set<FileMoverFile> allFileMoverFilesList, File exclusionFile) {
        this.allFileMoverFilesList = allFileMoverFilesList;
        this.exclusionFile = exclusionFile;
        this.excludedFolderNames = new ArrayList<>();
    }

    public void start() {
        parseExclusionFile();
        excludeFiles();
    }

    private void parseExclusionFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(exclusionFile))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                excludedFolderNames.add(line.trim());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void excludeFiles() {
        for (FileMoverFile fileMoverFile : allFileMoverFilesList) {
            File dir = fileMoverFile.getDirectory();
            if (isInExcludedFolder(dir)) {
                excludedFiles.add(fileMoverFile);
            } else {
                filesToDelete.add(fileMoverFile);
            }
        }
    }

    private boolean isInExcludedFolder(File dir) {
        for (String excludedFolderName : excludedFolderNames) {
            if (dir.getAbsolutePath().contains(excludedFolderName)
                    && dir.isDirectory()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCountDiscrepency() {
        return filesToDelete.size() + excludedFiles.size() != allFileMoverFilesList.size();
    }

    public List<String> getExcludedFolderNames() {
        return excludedFolderNames;
    }

    public Set<FileMoverFile> getFilesToDelete() {
        return filesToDelete;
    }

    public Set<FileMoverFile> getExcludedFiles() {
        return excludedFiles;
    }
}
