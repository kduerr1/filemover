/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover.util;

import com.google.common.base.Joiner;
import filemover.MoveMode;
import filemover.MoveMode;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author kduerr1
 */
public class CleanupReportBuilder {

    private final Stage stage;
    private final MoveMode currentMoveMode;
    private final List<String> cleanUpStrings;

    public CleanupReportBuilder(Stage stage, MoveMode currentMoveMode, List<String> cleanUpStrings) {
        this.stage = stage;
        this.currentMoveMode = currentMoveMode;
        this.cleanUpStrings = cleanUpStrings;
    }

    public boolean writeReport() {
        boolean isWritten = false;
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Save " + currentMoveMode + " Folder Report");
        chooser.setInitialFileName(currentMoveMode + "_cleanup_report");
        FileChooser.ExtensionFilter csvFilter = new FileChooser.ExtensionFilter(".csv", "CSV File");
        chooser.getExtensionFilters().add(csvFilter);

        File saveFile = chooser.showSaveDialog(stage);
        if (saveFile != null) {
            saveFile = new File(saveFile + ".csv");

            String outputString;
            Collections.sort(cleanUpStrings);
            outputString = Joiner.on("\n").join(cleanUpStrings);

            isWritten = saveFile(outputString, saveFile, isWritten);
        }
        return isWritten;
    }

    private boolean saveFile(String content, File saveFile, boolean isWritten) {

        try {
            FileWriter fileWriter = null;

            fileWriter = new FileWriter(saveFile);
            fileWriter.write(content);
            fileWriter.close();
            isWritten = true;
        } catch (IOException ex) {
            isWritten = false;
            ex.printStackTrace();
        }
        return isWritten;
    }

}
