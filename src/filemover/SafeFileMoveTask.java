/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author kduerr1
 */
public class SafeFileMoveTask {

    private File md5File = null;
    private File sourceFile;
    private final File destinationFile;
    private File destinationMD5File;

    private String sourceChecksum;
    private String destChecksum;

    public SafeFileMoveTask(FileMoverFile fileMoverFile) {
        this.sourceFile = fileMoverFile.getFile();
        this.destinationFile = fileMoverFile.getNewPath();
    }

    public SafeFileMoveTask() {
        this.sourceFile = null;
        this.destinationFile = null;
    }

    public void setSourceFile(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    public static void main(String[] args) {
        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask();
        File testFile = new File("\\\\jhgmnt.jhgenomics.jhu.edu\\research\\active\\Holland_MendelianDisorders_SeqWholeExome_033117_1\\CRAM\\125777-0224060962.cram.crai");
        fileMoveTask.setSourceFile(testFile);
        try {
            String md5 = DigestUtils.md5Hex(new BufferedInputStream(new FileInputStream(testFile)));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SafeFileMoveTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SafeFileMoveTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean performBamTask() throws Exception {
        sourceChecksum = getSourceBamMD5();

        if (!sourceFile.renameTo(destinationFile)) {
            new File(destinationFile.getAbsolutePath()).delete();
            sourceFile.renameTo(destinationFile);
        }
        destChecksum = getDestFileChecksum();
        writeDestinationMD5();

        deleteMD5();

        return isChecksumEqual();
    }

    public boolean performCramTask() throws Exception {
        sourceChecksum = getSourceCramMD5();

        if (!sourceFile.renameTo(destinationFile)) {
            new File(destinationFile.getAbsolutePath()).delete();
            sourceFile.renameTo(destinationFile);
        }
        destChecksum = getDestFileChecksum();
        writeDestinationMD5();

        deleteMD5();

        return isChecksumEqual();
    }

    public boolean performCramCraiTask() throws Exception {
        sourceChecksum = getSourceCramCraiMD5();

        if (!sourceFile.renameTo(destinationFile)) {
            new File(destinationFile.getAbsolutePath()).delete();
            sourceFile.renameTo(destinationFile);
        }
        destChecksum = getDestFileChecksum();
        writeDestinationMD5();

        deleteMD5();

        return isChecksumEqual();
    }

    private String getDestFileChecksum() throws IOException, NoSuchAlgorithmException, Exception {
        for (int i = 0; i < 5; i++) {
            try {
                String md5 = DigestUtils.md5Hex(new BufferedInputStream(new FileInputStream(destinationFile)));
                return md5;
            } catch (Exception e) {
                if (i == 5) {
                    throw e;
                }
            }
        }
        throw new Exception("Reached max retries for MD5 check :" + destinationFile.getAbsolutePath());
    }

    private boolean isChecksumEqual() throws Exception {
        if (sourceChecksum.equals(destChecksum) == false) {
            return false;
        } else {
            return true;
        }
    }

    boolean findMd5File() {
        this.md5File = new File(sourceFile.getParent(), sourceFile.getName() + ".md5");
        return md5File.exists();
    }

    boolean findCramCraiMd5File() {
        this.md5File = new File(sourceFile.getParent(), sourceFile.getName().replaceAll(".crai", "") + ".md5");
        return md5File.exists();
    }

    public File getDestinationMD5File() {
        return destinationMD5File;
    }

    private String getSourceBamMD5() throws FileNotFoundException {
        String md5 = "";

        this.md5File = new File(sourceFile.getParent(), sourceFile.getName() + ".md5");
        Scanner scanner = new Scanner(md5File);

        while (scanner.hasNext()) {
            md5 = scanner.nextLine();
        }

        scanner.close();

        return md5;
    }

    private String getSourceCramMD5() throws FileNotFoundException {
        String md5 = "";
        this.md5File = new File(sourceFile.getParent(), sourceFile.getName() + ".md5");
        Scanner scanner = new Scanner(md5File);

        while (scanner.hasNext()) {
            String firstLine = scanner.nextLine();
            md5 = firstLine.substring(0, firstLine.indexOf(" "));
            break;
        }

        scanner.close();
        return md5;
    }

    private String getSourceCramCraiMD5() throws FileNotFoundException {
        String md5 = "";
        this.md5File = new File(sourceFile.getParent(), sourceFile.getName().replaceAll(".crai", "") + ".md5");
        Scanner scanner = new Scanner(md5File);

        while (scanner.hasNext()) {
            String firstLine = scanner.nextLine();
            if (firstLine.endsWith(".cram.crai")) {
                md5 = firstLine.substring(0, firstLine.indexOf(" "));
            }
        }
        scanner.close();
        return md5;
    }

    public void writeMD5test() throws FileNotFoundException, IOException, NoSuchAlgorithmException {
        String file = "C:\\Users\\kduerr1.WIN\\Desktop\\File_Mover\\123456_TEST_123_DEF\\BAM";
//        String file = "\\\\isilon-cifs.cidr.jhu.edu\\seq_proj\\Egan_EDS_SeqWholeExome_090816_1\\Release_Data\\rawdataset_to_PI\\BAM";
        File dir = new File(file);
        List<File> files = new ArrayList<>();

        for (File listFile : dir.listFiles()) {
            if (listFile.getName().endsWith(".bam")) {
                files.add(listFile);
            }
        }

        for (File file1 : files) {
            String md5 = DigestUtils.md5Hex(new FileInputStream(file1));
            String filemd5 = file1.getAbsolutePath() + ".md5";
            File fileForMD5 = new File(filemd5);

            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileForMD5), "utf-8"))) {
                writer.write(md5);
            }
        }

//        String file = "\\\\isilon-cifs.cidr.jhu.edu\\seq_proj\\Egan_EDS_SeqWholeExome_090816_1\\Release_Data\\rawdataset_to_PI\\BAM\\108053-0224134612.bam";
//        String filemd5 = file + ".md5";
//        String filemd52 = "C:\\Users\\kduerr1.WIN\\Desktop\\testMD5_3.md5";
//"C:\\Users\\kduerr1.WIN\\Desktop\\File_Mover\\DESTINATION\\abc2\\BAM\\12345.bam"
//        File fileForMD5 = new File(file);
//        String md5 = DigestUtils.md5Hex(new BufferedInputStreamnew FileInputStream(fileForMD5)));
//        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
//                new FileOutputStream(filemd52), "utf-8"))) {
//            writer.write(md5);
//        }
    }

    private static List<File> count(File dir, String endsWith) {
        List<File> bamFiles = new ArrayList<>();
//        int count = 0;
        for (File listFile : dir.listFiles()) {
            if (listFile.isFile()) {
                if (listFile.getName().endsWith(endsWith)) {
//                    count++;
                    bamFiles.add(listFile);
                }
            } else {
                bamFiles.addAll(count(listFile, endsWith));
            }
        }
        return bamFiles;
    }

//    public static void main(String[] args) throws IOException {
////        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask();
////        try {
////            fileMoveTask.writeMD5test();
////        } catch (IOException ex) {
////            Logger.getLogger(SafeFileMoveTask.class.getName()).log(Level.SEVERE, null, ex);
////        } catch (NoSuchAlgorithmException ex) {
////            Logger.getLogger(SafeFileMoveTask.class.getName()).log(Level.SEVERE, null, ex);
////        }
//        File dir = new File("\\\\jhgmnt.jhgenomics.jhu.edu\\research\\active\\Holland_MendelianDisorders_SeqWholeExome_033117_1\\Release_Data\\Release_5\\Holland_WES_Release_5\\BAM");
//        File dir2 = new File("\\\\jhgmnt.jhgenomics.jhu.edu\\research\\active\\Holland_MendelianDisorders_SeqWholeExome_033117_1\\BAM\\AGGREGATE");
////        File dir = new File("R:\\active\\M_Valle_MendelianDisorders_SeqWholeExome_120511_75\\Release_Data\\rawdataset_to_PI");
////        File dir2 = new File("R:\\active\\M_Valle_MendelianDisorders_SeqWholeExome_120511_75\\BAM\\AGGREGATE");
//        List<File> bamFiles = new ArrayList<>();
//        List<File> md5s = new ArrayList<>();
//
//        bamFiles = count(dir, ".bam");
//        md5s = count(dir2, ".md5");
//
//        List<String> needsMD5 = new ArrayList<>();
////        needsMD5.add("143899-0224060572.bam");
//        needsMD5.add("143927-0224060543.bam");
////        needsMD5.add("143889-0224060587.bam");
//        needsMD5.add("143917-0224060553.bam");
//        needsMD5.add("143952-0224060602.bam");
//        needsMD5.add("143921-0224060549.bam");
//        needsMD5.add("143942-0224060527.bam");
//        needsMD5.add("143931-0224060534.bam");
////        needsMD5.add("143906-0224060557.bam");
////        needsMD5.add("143900-0224060571.bam");
//        needsMD5.add("143939-0224060530.bam");
//        needsMD5.add("143965-0224060581.bam");
//        needsMD5.add("143923-0224060547.bam");
//        needsMD5.add("143953-0224060601.bam");
////        needsMD5.add("137813-0224061035.bam");
////        needsMD5.add("143888-0224060586.bam");
//
//        for (File bamFile : bamFiles) {
//            boolean needs = false;
//            for (String string : needsMD5) {
//                if (bamFile.getName().contains(string)) {
//                    needs = true;
//
////                    System.out.println("md5 = " + bamFile);
//                }
//            }
//            if (needs) {
//                File md5F = new File("\\\\jhgmnt.jhgenomics.jhu.edu\\research\\active\\Holland_MendelianDisorders_SeqWholeExome_033117_1\\BAM\\AGGREGATE\\" + bamFile.getName() + ".md5");
////                File md5F = new File("R:\\active\\M_Valle_MendelianDisorders_SeqWholeExome_120511_75\\BAM\\AGGREGATE\\" + bamFile.getName() + ".md5");
//                String md51;
//                md51 = null;
//                try (Scanner scanner = new Scanner(md5F)) {
//                    while (scanner.hasNext()) {
//                        md51 = (scanner.nextLine());
//                        System.out.println("md51 = " + md51);
//                    }
//                } catch (Exception e) {
//
//                }
//                String md52 = null;
//                try {
//                    md52 = DigestUtils.md5Hex(new BufferedInputStream(new FileInputStream(bamFile)));
//                } catch (FileNotFoundException ex) {
//                    Logger.getLogger(SafeFileMoveTask.class.getName()).log(Level.SEVERE, null, ex);
//                }
//                System.out.println("bamFile = " + bamFile + " : " + md52);
//                System.out.println("md5 = " + md5F + " : " + md51);
//                System.out.println("PASS: " + md51.equals(md52));
//                System.out.println("");
//            }
//        }
//    }
    private void writeDestinationMD5() throws FileNotFoundException, IOException, NoSuchAlgorithmException {
        destinationMD5File = new File(destinationFile.getParent(), destinationFile.getName() + ".md5");
        String md5 = destChecksum;
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(destinationMD5File), "utf-8"))) {
            writer.write(md5);
        }
    }

    private void deleteMD5() {
        try {
            if (isChecksumEqual()) {
//                FileDeleteStrategy.FORCE.delete(destinationMD5File);
//                org.apache.commons.io.FileUtils.deleteQuietly(destinationMD5File);
                boolean delete = destinationMD5File.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
