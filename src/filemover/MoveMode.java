/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import java.util.EnumSet;
import java.util.Set;

/**
 * @author kduerr1
 */
public enum MoveMode {

    //ALL  .bam, .bai, _annovar_report.txt, .vcf, .tbi, and .gz
    ALL_FILES("All Files", FileType.ALL_FILES_FILE_TYPES, null, null),


    //QC
    QC_REPORT("QC Report", FileType.QC_REPORT_FILE_TYPES, null, null),

    QC_REPORT_BY_FAMILY("QC Report for Mendel Projects (sorted by family)", FileType.QC_REPORT_FAMILY_FILE_TYPES, null, null),


    //CLEAN UP
    SEQ_RUN_CLEAN_UP("HiSeq Clean Up", FileType.SEQ_RUN_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq"),

    MISEQ_RUN_CLEAN_UP("MiSeq Clean Up", FileType.MISEQ_RUN_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\miseq\\data", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\miseq\\data"),

    NOVA_CLEAN_UP("NovaSeq Clean Up", FileType.NOVA_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq"),

    BAD_FLOW_CELL_CLEAN_UP("HiSeq Bad Flowcell Clean Up", FileType.BAD_FLOW_CELL_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq\\Bad_Flow_Cells", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq\\Bad_Flow_Cells"),

    NOVA_BAD_FLOW_CELL_CLEAN_UP("NovaSeq Bad Flowcell Clean Up", FileType.NOVA_BAD_FLOW_CELL_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq\\Bad_Flow_Cells", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq\\Bad_Flow_Cells"),

    FAST_Q_CLEAN_UP("FASTQ Clean Up", FileType.FASTQ_CLEAN_UP_FILE_TYPES, "N:\\novaseq\\", "N:\\novaseq\\"),


    //BCL Clean Up
    SEQ_BCL_CLEAN_UP("BCL Clean Up (HiSeq)", FileType.BCL_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\hiseq"),

    MISEQ_BCL_CLEAN_UP("BCL Clean Up (MiSeq)", FileType.BCL_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\miseq\\data", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\miseq\\data"),

//    NOVA_BCL_CLEAN_UP("BCL Clean Up (NovaSeq)", FileType.BCL_CLEAN_UP_FILE_TYPES, "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq", "\\\\jhgmnt.jhgenomics.jhu.edu\\instrument_files\\novaseq\\"),

    CUSTOM_BCL_CLEAN_UP("BCL Clean Up (Custom)", FileType.BCL_CLEAN_UP_FILE_TYPES, "\\", "\\");


    private final String myName;
    private final Set<FileType> fileTypeSet;
    private final String initialDirectory;
    private final String finalDirectory;
    public static final Set<MoveMode> CLEAN_UP_MODES = EnumSet.of(SEQ_RUN_CLEAN_UP, MISEQ_RUN_CLEAN_UP, SEQ_BCL_CLEAN_UP, MISEQ_BCL_CLEAN_UP, BAD_FLOW_CELL_CLEAN_UP, CUSTOM_BCL_CLEAN_UP, NOVA_CLEAN_UP, NOVA_BAD_FLOW_CELL_CLEAN_UP, FAST_Q_CLEAN_UP);
    public static final Set<MoveMode> BCL_MODES = EnumSet.of(SEQ_BCL_CLEAN_UP, MISEQ_BCL_CLEAN_UP, CUSTOM_BCL_CLEAN_UP);
    public static final Set<MoveMode> STATIONARY_MODES = EnumSet.of(SEQ_BCL_CLEAN_UP, MISEQ_BCL_CLEAN_UP, BAD_FLOW_CELL_CLEAN_UP, CUSTOM_BCL_CLEAN_UP, NOVA_BAD_FLOW_CELL_CLEAN_UP, NOVA_CLEAN_UP, FAST_Q_CLEAN_UP);
    public static final Set<MoveMode> MOVE_AFTER_CLEAN = EnumSet.of(SEQ_RUN_CLEAN_UP, MISEQ_RUN_CLEAN_UP, BAD_FLOW_CELL_CLEAN_UP, NOVA_CLEAN_UP);
    public static final Set<MoveMode> BAD_FLOW_CELL_MODES = EnumSet.of(BAD_FLOW_CELL_CLEAN_UP, NOVA_BAD_FLOW_CELL_CLEAN_UP);


    MoveMode(String myName, Set<FileType> fileTypeSet, String initialDirectory, String finalDirectory) {
        this.myName = myName;
        this.fileTypeSet = fileTypeSet;
        this.initialDirectory = initialDirectory;
        this.finalDirectory = finalDirectory;
    }

    @Override
    public String toString() {
        return myName;
    }

    public String getMyName() {
        return myName;
    }

    public Set<FileType> getFileTypeSet() {
        return fileTypeSet;
    }

    public static Set<MoveMode> getCLEAN_UP_MODES() {
        return CLEAN_UP_MODES;
    }

    public String getInitialDirectory() {
        return initialDirectory;
    }

    public String getFinalDirectory() {
        return finalDirectory;
    }

    public static Set<MoveMode> getBCL_MODES() {
        return BCL_MODES;
    }

    public static Set<MoveMode> getSTATIONARY_MODES() {
        return STATIONARY_MODES;
    }

}
