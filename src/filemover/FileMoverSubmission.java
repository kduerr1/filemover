/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import com.google.common.base.Joiner;
import filemover.util.DeletedFilesLogger;
import filemover.util.MasterKeyReportHandler;
import filemover.util.DirectoryHandler;
import filemover.util.EmptyDirectoryCreator;
import filemover.util.FamilyDirectoryFileSorter;
import edu.jhmi.cidr.lib.file.FileUtils;
import filemover.util.logger.ParentLogger;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.io.FileDeleteStrategy;

/**
 *
 * @author kduerr1 1-10-2014
 */
class FileMoverSubmission {

    private final GridPane fileTypeGridPane;
    private final DirectoryHandler directoryHandler;
    private final Stage stage;
    private final AnchorPane anchorPane;
    private final ProgressBar progressBar;
    private final Pane userInputPane;
    private final Pane ModePane;
    private final Button submitButton;
    private final Label currentProgressFileLabel;
    private final Label percentProgressLabel;
    private Map<String, Set<FileMoverFile>> directoryToFileMoverFiles = new HashMap<>();
    private final List<FileMoverFile> errorFiles = new ArrayList<>();
    private MoveMode currentMoveMode = null;
    private boolean move = false;
    private boolean finishedRun = false;
    private boolean runningSubmission = false;
    private boolean copy = false;
    private boolean abort = false;
    private boolean completedSubmission = false;
    private double completeFileCount = 0;
    private File selectedDestination = null;
    private File phenoDBDir = null;
    private String currentProgressFileString;
    private List<FileMoverFile> completeFileMoverFileList;
    private final List<FileMoverFile> successfulFileMoverFileList;
    private Set<FileMoverFile> allFileMoverFilesList = new HashSet<>();
    private HashSet<String> cleanUpSelectedDirs;
    private FamilyDirectoryFileSorter familyDirectoryFileSorter;
    private MasterKeyReportHandler masterKeyReportHandler;
    private double fileCount;
    private final List<FileMoverFile> passedMD5 = new ArrayList<>();
    private final List<File> md5sToDelete = new ArrayList<>();
    private final List<FileMoverFile> failedMD5 = new ArrayList<>();
    private File customCleanUpDir = null;
    private String initialDirectoryString = "";
     private Map<FileType, Integer> fileTypeCountMap = new HashMap<>();
//    private Map<String, List<String>> projectToFamilyMap;
//    private final List<FileMoverFile> failedDeleteMD5 = new ArrayList<>();

    public FileMoverSubmission(GridPane fileTypeGridPane, DirectoryHandler directoryHandler,
            Stage stage, AnchorPane anchorPane, Pane userInputPane, Button submitButton,
            Pane ModePane, Label currentProgressFileLabel, Label percentProgressLabel,
            ProgressBar progressBar, Map<String, Set<FileMoverFile>> directoryToFileMoverFiles,
            Map<FileType, Integer> fileTypeCountMap) {

        this.successfulFileMoverFileList = new ArrayList<>();
        this.fileTypeGridPane = fileTypeGridPane;
        this.directoryHandler = directoryHandler;
        this.anchorPane = anchorPane;
        this.stage = stage;
        this.progressBar = progressBar;
        this.userInputPane = userInputPane;
        this.ModePane = ModePane;
        this.submitButton = submitButton;
        this.currentProgressFileLabel = currentProgressFileLabel;
        this.percentProgressLabel = percentProgressLabel;
//        this.familiesPrefixTextField = familiesPrefixTextField;
        this.directoryToFileMoverFiles = directoryToFileMoverFiles;
        this.fileTypeCountMap = fileTypeCountMap;
    }

    public void setUserData(MoveMode currentMoveMode, boolean move, boolean copy, String initialDirectoryString,
            File selectedDestination, Set<FileMoverFile> allFileMoverFilesList,
            MasterKeyReportHandler masterKeyReportHandler, FamilyDirectoryFileSorter familyDirectoryFileSorter,
            Map<String, List<String>> projectToFamilyMap,
            HashSet<String> cleanUpSelectedDirs, File customBclDir) {

        this.customCleanUpDir = customBclDir;
        this.currentMoveMode = currentMoveMode;
        this.move = move;
        this.copy = copy;
        this.initialDirectoryString = initialDirectoryString;
        this.selectedDestination = selectedDestination;
        this.allFileMoverFilesList = allFileMoverFilesList;
        this.masterKeyReportHandler = masterKeyReportHandler;
        this.familyDirectoryFileSorter = familyDirectoryFileSorter;
        this.cleanUpSelectedDirs = cleanUpSelectedDirs;
//        this.projectToFamilyMap = projectToFamilyMap;
    }

    public List<FileMoverFile> checkForDuplicates() {
        completeFileCount = 0;
        List<FileType> enabledFileTypes = new ArrayList<>();

        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            enabledFileTypes.addAll(currentMoveMode.getFileTypeSet());
        } else {
            for (Node box : fileTypeGridPane.getChildren()) {
                FileTypeBox fileTypeBox = (FileTypeBox) box;

                if (isCheckedFileType(fileTypeBox)) {
                    enabledFileTypes.add(fileTypeBox.getFileType());
                }
            }
        }

        completeFileMoverFileList = new ArrayList<>();

        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
            //add filemap to see if bams exist
            familyDirectoryFileSorter = new FamilyDirectoryFileSorter(
                    masterKeyReportHandler.getFamilyToSampleMap(),
                    masterKeyReportHandler.getProjectToFamilyMap(),
                    selectedDestination, fileTypeCountMap);
            for (FileMoverFile fileMoverFile : allFileMoverFilesList) {
                familyDirectoryFileSorter.translateFilemoverFile(fileMoverFile);
                fileMoverFile.setNewDestination(familyDirectoryFileSorter.getNewDestination());
                fileMoverFile.setNewPath(familyDirectoryFileSorter.getNewPath());
                fileMoverFile.setNewDirectory(familyDirectoryFileSorter.getNewDirectory());

                familyDirectoryFileSorter.getNewDirectory().mkdirs();
                completeFileMoverFileList.add(fileMoverFile);
                completeFileCount++;
            }
        } else {
            if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                directoryHandler.setMovingFileList(allFileMoverFilesList);
                if (customCleanUpDir != null) {
                    initialDirectoryString = customCleanUpDir.getAbsolutePath();
                } else {
                    initialDirectoryString = currentMoveMode.getInitialDirectory();

                }
            }
            for (FileMoverFile fileMoverFile : directoryHandler.getCheckedDirectoryFileList()) {
                if (enabledFileTypes.contains(fileMoverFile.getFileType())) {
                    if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                        fileMoverFile.setNewDestination(new File(selectedDestination.toString() + "\\" + fileMoverFile.getDirectory().getAbsolutePath().replace(initialDirectoryString, "")));
                        fileMoverFile.setNewPath(new File(selectedDestination.toString() + "\\" + fileMoverFile.getFile().getAbsolutePath().replace(initialDirectoryString, "")));
                        fileMoverFile.setNewDirectory(new File(fileMoverFile.getNewDestination().getAbsolutePath()));
                    }
                    completeFileMoverFileList.add(fileMoverFile);
                    completeFileCount++;
                }
            }
        }

        if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {

            for (FileMoverFile fileMoverFile : completeFileMoverFileList) {
                if (fileMoverFile.getNewPath().isFile()) {
                    errorFiles.add(fileMoverFile);
                }
            }
        }
        return errorFiles;
    }

    private void runTask() {
        setAllFieldsDisabled(true);
        if (!MoveMode.getSTATIONARY_MODES().contains(currentMoveMode)) {
            startMovingLoggerThread();
        }
        new Thread(() -> {
            fileCount = 0;

            if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                EmptyDirectoryCreator directoryCreator = new EmptyDirectoryCreator(selectedDestination);
                directoryCreator.createDirectories();
                phenoDBDir = directoryCreator.getPhenoDBDir();
            }

            completeFileMoverFileList.parallelStream().forEach(fmf -> processFileMoverFile(fmf));

            writeCleanUpLogs();

            if (!isAbort()) {
                completedSubmission = true;
                finishedRun = true;
                displayCompleteDialog();
            } else {
                if (completedSubmission) {
                    runningSubmission = false;
                    displayCompleteDialog();
                }
            }
        }).start();
    }

    public void submit() {
        completedSubmission = false;
        runningSubmission = true;
        List<String> finishedMessages = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
            sb.append("Processing ");
        } else if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            sb.append("Deleting ");
            copy = false;
            move = true;
        } else {
            if (move) {
                sb.append("Moving ");
            } else {
                sb.append("Copying ");
            }
        }
        sb.append((int) completeFileCount);
        sb.append(" file(s)");
        finishedMessages.add(sb.toString());
        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            finishedMessages.add("From selected directories ");
            if (!MoveMode.getSTATIONARY_MODES().contains(currentMoveMode)) {
                if (isActuallyMoving()) {
                    finishedMessages.add("Then moving file(s)");
                    finishedMessages.add("To: " + selectedDestination.toString());
                }

            }
        } else {
            finishedMessages.add("From: " + initialDirectoryString);
        }

        Alert submitAlert = new Alert(Alert.AlertType.CONFIRMATION);

        submitAlert.setContentText(Joiner.on("\n").join(finishedMessages));
        submitAlert.setHeaderText("Submit Task");
        submitAlert.setTitle("Confirm Submission");
        submitAlert.showAndWait().ifPresent(response -> {
            if (response == ButtonType.OK) {
                runTask();
            }
            if (response == ButtonType.CANCEL) {
                runningSubmission = false;
            }
        });
    }

//    private void copyBamFile(FileMoverFile fileMoverFile) {
//        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask(fileMoverFile);
//        try {
//            if (fileMoveTask.performTask()) {
//                passedMD5.add(fileMoverFile);
//            } else {
//                failedMD5.add(fileMoverFile);
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(FileMoverSubmission.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
    private void moveBamFile(FileMoverFile fileMoverFile) {
        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask(fileMoverFile);
        try {
            if (fileMoveTask.findMd5File()) {
                if (fileMoveTask.performBamTask()) {
                    passedMD5.add(fileMoverFile);
//                    md5sToDelete.add(fileMoveTask.getDestinationMD5File());
                } else {
                    failedMD5.add(fileMoverFile);
                }
            } else {
                System.out.println("NO BAM MD5: " + fileMoverFile.getFile().getAbsolutePath());
                failedMD5.add(fileMoverFile);
            }
        } catch (Exception ex) {
            Logger.getLogger(FileMoverSubmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void moveCramFile(FileMoverFile fileMoverFile) {
        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask(fileMoverFile);
        try {
            if (fileMoveTask.findMd5File()) {
                if (fileMoveTask.performCramTask()) {
                    passedMD5.add(fileMoverFile);
                } else {
                    failedMD5.add(fileMoverFile);
                }
            } else {
                System.out.println("NO CRAM MD5: " + fileMoverFile.getFile().getAbsolutePath());
                failedMD5.add(fileMoverFile);
            }
        } catch (Exception ex) {
            Logger.getLogger(FileMoverSubmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void moveCramCraiFile(FileMoverFile fileMoverFile) {
        SafeFileMoveTask fileMoveTask = new SafeFileMoveTask(fileMoverFile);
        try {
            if (fileMoveTask.findCramCraiMd5File()) {
                if (fileMoveTask.performCramCraiTask()) {
                    passedMD5.add(fileMoverFile);
                } else {
                    failedMD5.add(fileMoverFile);
                }
            } else {
                System.out.println("NO CRAM CRAI MD5: " + fileMoverFile.getFile().getAbsolutePath());
                failedMD5.add(fileMoverFile);
            }
        } catch (Exception ex) {
            Logger.getLogger(FileMoverSubmission.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void startMovingLoggerThread() {
        List<File> movingDirectories = new ArrayList<>();
        for (String directory : cleanUpSelectedDirs) {
            if (selectedDestination != null) {
                movingDirectories.add(new File(selectedDestination.getAbsolutePath(), directory));
            } else {
                movingDirectories.add(new File(currentMoveMode.getFinalDirectory(), directory));
            }
        }
        ParentLogger.start(movingDirectories);
    }

    private boolean isActuallyMoving() {
        for (final String directoryString : cleanUpSelectedDirs) {
            String inString;
            String outString;

            if (customCleanUpDir != null) {
                inString = customCleanUpDir.getAbsolutePath() + "\\" + directoryString;
            } else {
                inString = currentMoveMode.getInitialDirectory() + "\\" + directoryString;
            }

            if (selectedDestination != null) {
                outString = selectedDestination.getAbsolutePath() + "\\" + directoryString;
            } else {
                outString = currentMoveMode.getFinalDirectory() + "\\" + directoryString;
            }

            if (inString.equals(outString)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCheckedFileType(FileTypeBox fileTypeBox) {
        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)
                || currentMoveMode.equals(MoveMode.QC_REPORT)) {
            return true;
        }
        VBox vBox = (VBox) fileTypeBox.getChildren().get(0);
        HBox hBox = (HBox) vBox.getChildren().get(1);
        CheckBox checkBox = (CheckBox) hBox.getChildren().get(1);
        return checkBox.isSelected();
    }

    public void updateProgressLabel() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                currentProgressFileLabel.setVisible(true);
                if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                    currentProgressFileLabel.setText("Processing: " + currentProgressFileString);
                } else if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                    currentProgressFileLabel.setText("Deleting: " + currentProgressFileString);
                    if (!MoveMode.getSTATIONARY_MODES().contains(currentMoveMode)) {

                    }
                } else {
                    if (copy) {
                        currentProgressFileLabel.setText("Copying: " + currentProgressFileString);
                    }
                    if (move) {
                        currentProgressFileLabel.setText("Moving: " + currentProgressFileString);
                    }
                }
                if (progressBar.getProgress() != -1) {
                    percentProgressLabel.setText(String.valueOf((int) (progressBar.getProgress() * 100)) + "%");
                }
            }
        });
    }

    private void setAllFieldsDisabled(final boolean enable) {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                anchorPane.setCursor(Cursor.WAIT);
                userInputPane.setDisable(enable);
                submitButton.setDisable(enable);
                ModePane.setDisable(enable);
                progressBar.setVisible(enable);
                progressBar.setProgress(-1);
                percentProgressLabel.setText("0");
                currentProgressFileLabel.setVisible(enable);
                percentProgressLabel.setVisible(enable);
            }
        });
    }

    public void setAbort(boolean abort) {
        this.abort = abort;
    }

    public boolean isAbort() {
        return abort;
    }

    public List<FileMoverFile> getSuccessfulFileMoverFileList() {
        return successfulFileMoverFileList;
    }

    public void setCurrentProgressFileString(String currentProgressFileString) {
        this.currentProgressFileString = currentProgressFileString;
    }

    public boolean isCompleted() {
        return completedSubmission;
    }

    private void displayCompleteDialog() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);

                anchorPane.setCursor(Cursor.DEFAULT);
                List<String> completionStrings = new ArrayList<>();

                if (!passedMD5.isEmpty()) {
                    completionStrings.add(passedMD5.size() + " file(s) passed MD5 validation");
                    completionStrings.add(failedMD5.size() + " file(s) failed MD5 validation");
                }
                if (!failedMD5.isEmpty()) {
                    completionStrings.add(passedMD5.size() + " file(s) passed MD5 validation");
                    completionStrings.add(failedMD5.size() + " file(s) failed MD5 validation");
                }
//                if (!failedDeleteMD5.isEmpty()) {
//                    completionStrings.add(passedMD5.size() + " file(s) passed MD5 validation and were moved, but the original file(s) would not delete");
//                }

                completionStrings.add("\nThe Application will now close");

                ListView<String> failedListView = new ListView<>();
                ListView<String> passedListView = new ListView<>();

                failedListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                passedListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

                MenuItem copyFail = new MenuItem("Copy");
                MenuItem copyFailAll = new MenuItem("Copy All");
                ContextMenu contextFailMenu = new ContextMenu(copyFail, copyFailAll);

                MenuItem copyPass = new MenuItem("Copy");
                MenuItem copyPassAll = new MenuItem("Copy All");
                ContextMenu contextPassMenu = new ContextMenu(copyPass, copyPassAll);

                failedListView.setContextMenu(contextFailMenu);
                passedListView.setContextMenu(contextPassMenu);

                copyFail.setOnAction((ActionEvent event) -> {
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(Joiner.on("\n").join(failedListView.getSelectionModel().getSelectedItems()));
                    Clipboard.getSystemClipboard().setContent(content);
                });
                copyFailAll.setOnAction((ActionEvent event) -> {
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(Joiner.on("\n").join(failedListView.getItems()));
                    Clipboard.getSystemClipboard().setContent(content);
                });
                copyPass.setOnAction((ActionEvent event) -> {
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(Joiner.on("\n").join(passedListView.getSelectionModel().getSelectedItems()));
                    Clipboard.getSystemClipboard().setContent(content);
                });
                copyPassAll.setOnAction((ActionEvent event) -> {
                    final ClipboardContent content = new ClipboardContent();
                    content.putString(Joiner.on("\n").join(passedListView.getItems()));
                    Clipboard.getSystemClipboard().setContent(content);
                });

                failedListView.setMaxHeight(200);
                failedListView.setMinWidth(600);
                passedListView.setMaxHeight(200);

                passedMD5.forEach((f) -> {
                    passedListView.getItems().add(f.getFile().getAbsolutePath());
                });
                failedMD5.forEach((f) -> {
                    failedListView.getItems().add(f.getFile().getAbsolutePath());
                });

                VBox vb = new VBox(
                        new Label(" " + passedMD5.size() + " Passed MD5"),
                        passedListView,
                        new Label(" " + failedMD5.size() + " Failed MD5"),
                        failedListView);

                alert.setContentText(Joiner.on("\n").join(completionStrings));
                alert.setHeaderText("Task Complete");
                alert.setTitle("File Mover Task Complete");
                alert.getDialogPane().setExpandableContent(vb);
                alert.getDialogPane().setExpanded(false);
                alert.getButtonTypes().setAll(ButtonType.CLOSE);
                if (alert.showAndWait().isPresent()) {
                    System.exit(0);
                }
            }
        });
    }

    void setFinishedRun(boolean b) {
        finishedRun = b;
    }

    public boolean isFinishedRun() {
        return finishedRun;
    }

    public void setCompleted(boolean completed) {
        this.completedSubmission = completed;
    }

    public boolean isRunning() {
        return runningSubmission;
    }

    private void updateProgress() {
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setProgress(fileCount / completeFileCount);
            }
        });
    }

    public List<String> getBamFilesMissingMD5s() {
        List<String> missingMD5s = new ArrayList<>();
        for (FileMoverFile fileMoverFile : allFileMoverFilesList) {
            if (fileMoverFile.getFileType().equals(FileType.BAM) || fileMoverFile.getFileType().equals(FileType.CRAM)) {
                File md5File = new File(fileMoverFile.getDirectory(), fileMoverFile.getFile().getName() + ".md5");
                if (!md5File.exists()) {
                    missingMD5s.add(fileMoverFile.getFile().getAbsolutePath());
                }
            }
        }

        return missingMD5s;
    }

    private void createNonExistantDirs(FileMoverFile fileMoverFile) {
        if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            if (!fileMoverFile.getNewDirectory().exists()) {
                fileMoverFile.getNewDestination().mkdirs();
            }
        }
    }

    private void manageFile(FileMoverFile fileMoverFile) throws Exception {
        if (move) {
            if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                boolean delete;
                if (FileMoverController.TESTING_MODE == false) {
                    delete = fileMoverFile.getFile().delete();
                    if (!delete) {
                        System.out.println("ERROR DELETING:" + fileMoverFile.getFile().getAbsolutePath());
                    }
                }
            } else {
                fileMoverFile.setMove(true);
                if (fileMoverFile.getFileType().equals(FileType.BAM)) {
                    moveBamFile(fileMoverFile);
                } else if (fileMoverFile.getFileType().equals(FileType.CRAM)) {
                    moveCramFile(fileMoverFile);
                } else if (fileMoverFile.getFileType().equals(FileType.CRAM_CRAI)) {
                    moveCramCraiFile(fileMoverFile);
                } else {
                    if (!fileMoverFile.getFile().renameTo(fileMoverFile.getNewPath())) {
                        fileMoverFile.getNewPath().delete();
                        fileMoverFile.getFile().renameTo(fileMoverFile.getNewPath());
                    }
                }
            }
        }
        if (copy) {
            if (fileMoverFile.getFileType().equals(FileType.BAM)) {
                moveBamFile(fileMoverFile);
            } else if (fileMoverFile.getFileType().equals(FileType.CRAM)) {
                moveCramFile(fileMoverFile);
            } else if (fileMoverFile.getFileType().equals(FileType.CRAM_CRAI)) {
                moveCramCraiFile(fileMoverFile);
            } else {
                fileMoverFile.setMove(false);
                FileUtils.copy(fileMoverFile.getFile(), fileMoverFile.getNewPath(), FileUtils.OverwritePolicy.YES);
            }
        }
        updateProgressLabel();
    }

    private void manageMendelFile(FileMoverFile fileMoverFile) throws Exception {
        if (fileMoverFile.getFileType().equals(FileType.BAM)) {
            fileMoverFile.setMove(true);
            moveBamFile(fileMoverFile);
        } else if (fileMoverFile.getFileType().equals(FileType.CRAM)) {
            fileMoverFile.setMove(true);
            moveCramFile(fileMoverFile);
        } else if (fileMoverFile.getFileType().equals(FileType.CRAM_CRAI)) {
            fileMoverFile.setMove(false);
            moveCramCraiFile(fileMoverFile);
        } else {
            fileMoverFile.setMove(false);
            FileUtils.copy(fileMoverFile.getFile(), fileMoverFile.getNewPath(), FileUtils.OverwritePolicy.YES);

            if (fileMoverFile.isPhenoDBFile()) {
                File phonoCopyFile = new File(phenoDBDir, fileMoverFile.getFile().getName());
                FileUtils.copy(fileMoverFile.getFile(), phonoCopyFile, FileUtils.OverwritePolicy.YES);
            }
        }
        updateProgressLabel();
    }

    private void writeCleanUpLogs() {
        if (MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(-1);
                    percentProgressLabel.setText("");
                    currentProgressFileLabel.setText("Writing Clean Up Logs");
                }
            });
            try {
                final DeletedFilesLogger logger = new DeletedFilesLogger(directoryToFileMoverFiles, currentMoveMode, cleanUpSelectedDirs);

                if (MoveMode.CUSTOM_BCL_CLEAN_UP.equals(currentMoveMode)) {
                    logger.setCustomBclDir(customCleanUpDir);
                }

                logger.writeLogs();
            } catch (IOException ex) {
                System.out.println("ERROR: Writing Logs Files");
            }
            directoryToFileMoverFiles.clear();
            final DirectoryManager mover = new DirectoryManager(currentMoveMode, cleanUpSelectedDirs, progressBar, currentProgressFileLabel, percentProgressLabel, selectedDestination, stage);
            mover.setCustomBclDir(customCleanUpDir);
            try {
                if (mover.move()) {
                    completedSubmission = true;
                    finishedRun = true;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
//                                }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisible(false);
                    percentProgressLabel.setVisible(false);
                    currentProgressFileLabel.setVisible(false);
                }
            });
        }
    }

    private void processFileMoverFile(FileMoverFile fileMoverFile) {
        currentProgressFileString = fileMoverFile.getFile().getName();
        updateProgress();
        updateProgressLabel();
        if (!isAbort()) {
            createNonExistantDirs(fileMoverFile);
            try {
                if (!currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY)) {
                    manageFile(fileMoverFile);
                } else {
                    manageMendelFile(fileMoverFile);
                }

                successfulFileMoverFileList.add(fileMoverFile);

                if (isAbort()) {
//                            break;
                } else {

                    updateProgress();
                    updateProgressLabel();
                    fileCount++;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
//                    break;
        }
    }

}
