/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author kduerr1
 */
public class FileTypeBox extends VBox {

    private final FileType fileType;
    private final MoveMode currentMoveMode;
    public boolean selected = true;
    private CheckBox checkBox = new CheckBox();
    private int count;
    private FileTypeBox fileTypeBox;

    public FileTypeBox(FileType fileType, MoveMode currentMoveMode) {
        this.fileType = fileType;
        this.currentMoveMode = currentMoveMode;
        checkBox.addEventHandler(ActionEvent.ACTION, new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                if (!checkBox.isSelected()) {
                    fileTypeBox.setStyle("-fx-background-color:linear-gradient(to bottom, rgba(254,187,187,1) 0%,rgba(254,144,144,1) 45%,rgba(255,92,92,1) 100%);");
                } else {
//                    fileTypeBox.setStyle("-fx-background-color:linear-gradient(to bottom, rgba(210,255,82,1) 0%,rgba(145,232,66,1) 100%);");
                    fileTypeBox.setStyle("-fx-background-color:linear-gradient(to bottom, rgba(216,242,152,1) 0%,rgba(152,209,73,1) 100%);");
                }
            }
        });
    }

    public FileTypeBox createFileTypeBox() {
        fileTypeBox = new FileTypeBox(fileType, currentMoveMode);
        fileTypeBox.setStyle("-fx-background-color:linear-gradient(to bottom, rgba(216,242,152,1) 0%,rgba(152,209,73,1) 100%);");

        VBox fileTypeVBox = new VBox();
        HBox fileTypeHBox = new HBox();
        HBox countHBox = new HBox();
        Pane bufferPaneAboveHBox = new Pane();
        Pane bufferPaneBelowHBox = new Pane();
        Pane bufferPaneBeforeCheckBox = new Pane();
        Pane bufferPaneBeforeCountLabel = new Pane();

        VBox.clearConstraints(fileTypeBox);
        VBox.setVgrow(fileTypeVBox, Priority.ALWAYS);
        HBox.setHgrow(fileTypeHBox, Priority.ALWAYS);

//        bufferPaneAboveHBox.setMinHeight(6);
//        bufferPaneBelowHBox.setMinHeight(3);
        bufferPaneBeforeCheckBox.setMinWidth(10);
        bufferPaneBeforeCountLabel.setMinWidth(30);

        fileTypeVBox.setStyle("-fx-border-color: GREY;");
        Label fileTypeTextField = new Label(fileType.getExtension());
        Label introToCount = new Label(" File count: ");
        Label countLabel = new Label(String.valueOf(count));

        if (currentMoveMode.equals(MoveMode.QC_REPORT_BY_FAMILY) || currentMoveMode.equals(MoveMode.QC_REPORT)) {
//            fileTypeBox.setMinHeight(49);
//            fileTypeBox.setMaxHeight(49);
            bufferPaneBeforeCheckBox.setMinWidth(5);
            bufferPaneBeforeCountLabel.setMinWidth(5);

            fileTypeTextField.setFont(new Font("System", 16));
            introToCount.setFont(new Font("System", 14));
            countLabel.setFont(new Font("System", 14));

            fileTypeBox.setMinWidth(225);
            fileTypeBox.setMaxWidth(225);

            fileTypeHBox.getChildren().add(bufferPaneBeforeCheckBox);
            fileTypeHBox.getChildren().add(fileTypeTextField);

        } else {
            fileTypeTextField.setFont(new Font("System", 16));
            introToCount.setFont(new Font("System", 14));
            countLabel.setFont(new Font("System", 14));

            fileTypeHBox.getChildren().add(bufferPaneBeforeCheckBox);
            fileTypeTextField.setStyle(null);

            if (!MoveMode.getCLEAN_UP_MODES().contains(currentMoveMode)) {
                fileTypeHBox.getChildren().add(checkBox);
                checkBox.setSelected(selected);
            } else {
                Image skull = new Image(getClass().getResource("Skull_and_crossbones.png").toExternalForm());
                fileTypeHBox.getChildren().add(new ImageView(skull));
                        bufferPaneBeforeCountLabel.setMinWidth(17);

                countHBox.getChildren().add(bufferPaneBeforeCheckBox);
                fileTypeBox.setStyle("-fx-background-color:linear-gradient(to bottom, rgb(76,76,76) 0%,rgb(19,19,19) 100%);");
                introToCount.setStyle("-fx-text-fill: WHITE");
                countLabel.setStyle("-fx-text-fill: WHITE");
                fileTypeTextField.setStyle("-fx-text-fill: WHITE");
            }
            fileTypeHBox.getChildren().add(fileTypeTextField);
                countHBox.getChildren().add(bufferPaneBeforeCountLabel);
        }
        countHBox.getChildren().add(introToCount);
        countHBox.getChildren().add(countLabel);

        fileTypeVBox.getChildren().add(bufferPaneAboveHBox);
        fileTypeVBox.getChildren().add(fileTypeHBox);
        fileTypeVBox.getChildren().add(countHBox);
        fileTypeVBox.getChildren().add(bufferPaneBelowHBox);

        fileTypeBox.getChildren().add(fileTypeVBox);

        return fileTypeBox;
    }

    public FileType getFileType() {
        return fileType;
    }
}
