/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

/**
 *
 * @author kduerr1
 */
public class Job {

    private final String directory;
    private boolean cancelJob;
    private final JobRunnable runnable;

    public Job(String directory, JobRunnable runnable) {
        this.directory = directory;
        this.cancelJob = false;
        this.runnable = runnable;
    }

    public String getDirectory() {
        return directory;
    }

    public boolean isCancelJob() {
        return cancelJob;
    }

    public void cancelJob() {
        this.cancelJob = true;
    }

    @Override
    public String toString() {
        return String.valueOf(isCancelJob()); //To change body of generated methods, choose Tools | Templates.
    }

    public void run() {
        runnable.run(this);
    }

    public static interface JobRunnable {
        public void run(Job job);
    }
}
