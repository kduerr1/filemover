/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import java.util.List;
import java.util.Map;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

/**
 *
 * @author kduerr1
 */
public class ProjectBox extends VBox {
    private final Map<String, List<String>> projectToFamilyMap;
    private final Map<String, List<String>> projectToSampleMap;
    ProjectBox projectBox;

    public ProjectBox(Map<String, List<String>> projectToFamilyMap, Map<String, List<String>> projectToSampleMap) {
        this.projectToFamilyMap = projectToFamilyMap;
        this.projectToSampleMap = projectToSampleMap;
    }

    public ProjectBox createProjectBox(String project) {
        String samples = "";
        String families = "";
        
        for (String sample : projectToSampleMap.get(project)) {
            samples = (samples + sample + "\n");
        }

        for (String family : projectToFamilyMap.get(project)) {
            families = (families + family + "\n");
        }

        projectBox = new ProjectBox(projectToFamilyMap, projectToSampleMap);
        int sampleCount = projectToSampleMap.get(project).size();
        int familyCount = projectToFamilyMap.get(project).size();
        projectBox.setStyle("-fx-background-color: linear-gradient(to bottom, rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%);");
        projectBox.setMinWidth(897);
//        projectBox.setMinWidth(609);
        VBox projectStatCountVBox = new VBox();
        HBox projectHBox = new HBox();
        VBox projectNameVBox = new VBox();
        VBox samplesAndFamiliesCountVBox = new VBox();
        Pane bufferPaneAboveHBox = new Pane();
        Pane bufferPaneBelowHBox = new Pane();
        Pane bufferPaneBeforeProjectName = new Pane();
        Pane bufferPaneBeforeCountLabel = new Pane();
        Pane bufferPaneAboveProjectNameLabel = new Pane();

        VBox.clearConstraints(projectBox);
        VBox.setVgrow(projectStatCountVBox, Priority.ALWAYS);
        HBox.setHgrow(projectHBox, Priority.ALWAYS);

        bufferPaneBeforeProjectName.setMinWidth(10);
        bufferPaneBeforeCountLabel.setMinWidth(10);
        bufferPaneAboveProjectNameLabel.setMinHeight(5);

        projectStatCountVBox.setStyle("-fx-border-color: GREY;");
        Label projectNameLabel = new Label(project);
        Label familyCountLabel = new Label("Families: " + familyCount);
        familyCountLabel.setTooltip(new Tooltip(families));
        Label sampleCountLabel = new Label(String.valueOf("Samples: " + sampleCount));
        sampleCountLabel.setTooltip(new Tooltip(samples));

        projectNameLabel.setFont(new Font("System", 16));
        projectNameLabel.setMinWidth(770);
//        projectNameLabel.setMaxWidth(700);
        familyCountLabel.setFont(new Font("System", 11.5));
        sampleCountLabel.setFont(new Font("System", 11.5));

        projectHBox.getChildren().add(bufferPaneBeforeProjectName);
        projectNameVBox.getChildren().add(bufferPaneAboveProjectNameLabel);
        projectNameVBox.getChildren().add(projectNameLabel);
        projectHBox.getChildren().add(projectNameVBox);
        projectHBox.getChildren().add(bufferPaneBeforeCountLabel);
        samplesAndFamiliesCountVBox.getChildren().add(familyCountLabel);
        samplesAndFamiliesCountVBox.getChildren().add(sampleCountLabel);
        projectHBox.getChildren().add(samplesAndFamiliesCountVBox);

        projectStatCountVBox.getChildren().add(bufferPaneAboveHBox);
        projectStatCountVBox.getChildren().add(projectHBox);
        projectStatCountVBox.getChildren().add(bufferPaneBelowHBox);

        projectBox.getChildren().add(projectStatCountVBox);

        return projectBox;
    }

}
