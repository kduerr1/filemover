/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filemover;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiDialog;
import filemover.util.logger.ParentLogger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javafx.stage.Stage;

/**
 *
 * @author kduerr1
 */
public class DirectoryMover {

    private final MoveMode currentMoveMode;
    private final HashSet<String> cleanUpSelectedDirs;
    private final ProgressBar progressBar;
    private final Label currentProgressLabel;
    private final Label percentageLabel;
    private boolean running = false;
//    private File outputFile;
    private Stage stage;
    private List<Double> bytesPerMinuteList = new ArrayList<>();
    private List<String> cleanedUpDirs = new ArrayList<>();
    double totalBytes = 0;
    double progressBytes = 0;
//    private List<FileMoverFile> completeFileMoverFileList;
    private final File selectedDestination;
    private String initialDirectoryString;
    private String remainingTimeString = "";

    public DirectoryMover(MoveMode currentMoveMode,
            HashSet<String> cleanUpSelectedDirs, ProgressBar progressBar,
            Label currentProgressLabel, Label percentageLabel,
            File selectedDestination, Stage stage) {

        this.currentMoveMode = currentMoveMode;
        this.cleanUpSelectedDirs = cleanUpSelectedDirs;
        this.progressBar = progressBar;
        this.currentProgressLabel = currentProgressLabel;
        this.percentageLabel = percentageLabel;
        this.selectedDestination = selectedDestination;
        this.stage = stage;
    }

    public boolean move() throws IOException {
        boolean sucess = false;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                progressBar.setProgress(-1);
                percentageLabel.setText("");
                currentProgressLabel.setText("Deleting Empty Directories");
            }
        });

        for (String string : cleanUpSelectedDirs) {

            for (FileType fileType : currentMoveMode.getFileTypeSet()) {
                String directory = currentMoveMode.getInitialDirectory() + "\\" + string + "\\" + fileType.getExtension();
                File emptyDir = new File(directory);

                try {
                    if (emptyDir.exists()) {
                        if (getDirBytes(directory) == 0) {
                            FileUtils.forceDelete(emptyDir);
                        }
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        if (MoveMode.getSTATIONARY_MODES().contains(currentMoveMode)) {
            return true;
        } else {

//            try {
//                totalBytes = getTotalBytes();
//            } catch (IOException ex) {
//                Logger.getLogger(DirectoryMover.class.getName()).log(Level.SEVERE, null, ex);
//            }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    if (totalBytes != 0) {
                        progressBar.setProgress(-1);
                        percentageLabel.setText("");
                        currentProgressLabel.setText("Preparing Move...");
                    }
                }
            });

            for (final String directoryString : cleanUpSelectedDirs) {
//                long startTime = System.currentTimeMillis();
                System.out.println("Preparing " + directoryString);
                running = true;
                String inString = currentMoveMode.getInitialDirectory() + "\\" + directoryString;
                File dir = new File(inString);
                String outString = currentMoveMode.getFinalDirectory() + "\\" + directoryString;
                File outDir = new File(outString);
//                final double initialBytes = getDirBytes(inString);
                if (FileMoverController.TESTING_MODE == true) {
                    FileUtils.copyDirectory(dir, outDir);
                } else {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
//                            if (totalBytes != 0) {
                            double progress = (double) cleanedUpDirs.size() / cleanUpSelectedDirs.size();
                            progressBar.setProgress(progress);
                            percentageLabel.setText(String.valueOf((int) (progressBar.getProgress() * 100)) + "%");
//                                if ("".equals(remainingTimeString)) {
                            currentProgressLabel.setText("Moving: " + directoryString + " to " + currentMoveMode.getFinalDirectory());
//                                } else {
//                                    currentProgressLabel.setText("Moving: " + directoryString + " to " + currentMoveMode.getFinalDirectory() + " (" + readableFileSize(initialBytes) + ")" + "   :  Estimated Minutes Reamining " + remainingTimeString);
//                                }
//                            }
                        }
                    });
                    System.out.println("Starting move for " + directoryString);
                    try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir.toPath())) {
                        for (Path entry : stream) {
                            File file = entry.toFile();
                            moveEverything(file, outDir);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
//                            }
                    long dirSize = FileUtils.sizeOfDirectory(dir);
                    System.out.println("dirSize = " + dirSize);
                    if (dirSize == 0) {
//                        double finalBytes = getDirBytes(inString);
                        System.out.println("Finishing move and deleting " + directoryString);

                        deleteDir(dir);
//                        } else {
//                            }
                    } else {
                        System.out.println("ERROR: Could not move " + directoryString);

                    }
                }

//                progressBytes = progressBytes + getDirBytes(currentMoveMode.getFinalDirectory() + "\\" + directoryString);
//                String dirLogString = null;
//                for (Map.Entry<String, String> entry : ParentLogger.getDirToLogMap().entrySet()) {
//                    String dirName = entry.getKey();
//                    String logFileString = entry.getValue();
//                    if (dirName.equals(directoryString)) {
//                        dirLogString = logFileString;
//                    }
//                }
//                if (dirLogString != null) {
//                    double logBytes = getDirBytes(currentMoveMode.getFinalDirectory() + "\\" + dirLogString);
//                    progressBytes = progressBytes - logBytes;
//                }
//
//                double endTime = System.currentTimeMillis();
//                double elapsedTime = endTime - startTime;
//                if (elapsedTime != 0) {
//
//                    double bytesPerMilli = initialBytes / elapsedTime;
//                    bytesPerMinuteList.add(bytesPerMilli * 1000);
//                }
//                double total = 0;
//                for (double bytesPerMin : bytesPerMinuteList) {
//                    total = total + bytesPerMin;
//                }
//                double avgBytesPerMin = total / bytesPerMinuteList.size();
//                double remainingBytes = totalBytes - progressBytes;
//                if (remainingBytes != 0) {
//                    double remainingTime = ((remainingBytes / avgBytesPerMin) / 60);
//                    DecimalFormat df = new DecimalFormat("#");
//                    remainingTimeString = df.format(remainingTime);
//                }
                cleanedUpDirs.add(directoryString);
            }
            running = false;
            sucess = true;
            return sucess;
        }
    }

//    public double getTotalBytes() throws IOException {
//        double allBytes = 0;
//        for (String string : cleanUpSelectedDirs) {
//            allBytes += getDirBytes(currentMoveMode.getInitialDirectory() + "\\" + string);
//        }
//        System.out.println("allBytes = " + allBytes);
//        return allBytes;
//    }
    public static String readableFileSize(double size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "kB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public double getDirBytes(String string) throws IOException {
        double allBytes = 0;
        File file = new File(string);
        String directoryPath = file.getAbsolutePath();
        Process p = Runtime.getRuntime().exec("cmd /C dir /s /a " + directoryPath);
        BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        boolean introLine = false;
        String line;

        while ((line = input.readLine()) != null) {
            if (introLine == true) {
                String byteString = line.substring(line.indexOf(")") + 2, line.indexOf("bytes") - 1);
                byteString = byteString.trim();
                byteString = byteString.replaceAll(",", "");
                long bytes = Long.valueOf(byteString).longValue();
                allBytes += bytes;
                introLine = false;
            }
            if (line.contains("Total Files Listed:")) {
                introLine = true;
            }
        }
        input.close();
        while ((line = error.readLine()) != null) {
        }
        error.close();
        return allBytes;
    }

    private void deleteDir(final File dir) {
        try {
            FileUtils.deleteDirectory(dir);
        } catch (IOException ex) {
            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    GuiDialog dialog = new GuiDialog(stage);
                    dialog.showDialog("Please close " + dir.getAbsolutePath() + "\nand any of its files\nso it can be deleted.\n\nPress OK when finished.", GuiDialog.GuiDialogButton.OK);
                    dialog.setCallBackOnResponse(GuiDialog.Response.OK, new Runnable() {

                        @Override
                        public void run() {
                            deleteDir(dir);
                        }
                    });
                }
            });
        }
    }

    private void moveEverything(File file, File logOutDir) {
        if (file.isDirectory()) {

            try (DirectoryStream<Path> stream = Files.newDirectoryStream(file.toPath())) {
                for (Path entry : stream) {
                    File moveFile = entry.toFile();
                    moveEverything(moveFile, logOutDir);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            // Another Stream?
//            for (File directory : file.listFiles()) {
//                moveEverything(directory, logOutDir);
//            }
        } else {

            initialDirectoryString = currentMoveMode.getInitialDirectory();
            FileMoverFile fileMoverFile = new FileMoverFile(file, FileType.ANY);
            fileMoverFile.setNewDestination(new File(selectedDestination.toString() + "\\" + fileMoverFile.getDirectory().getAbsolutePath().replace(initialDirectoryString, "")));
            fileMoverFile.setNewPath(new File(selectedDestination.toString() + "\\" + fileMoverFile.getFile().getAbsolutePath().replace(initialDirectoryString, "")));
            if (!fileMoverFile.getNewDestination().exists()) {
                fileMoverFile.getNewDestination().mkdirs();
            }
            if (!fileMoverFile.getFile().renameTo(fileMoverFile.getNewPath())) {
                fileMoverFile.getNewPath().delete();
                fileMoverFile.getFile().renameTo(fileMoverFile.getNewPath());
            }

//          WRITE TO LOGGER
            if (file.getAbsolutePath() != null) {
                ParentLogger.addToLog(file.getAbsolutePath(), logOutDir);
            }
        }
    }
}
